Thesis Scope
=== Front ( User ) ===

    User Account
    Cart ( Member / Guest Account )
    Inquiry
    Recent Orders
    Checkout
    Discount
    Order Status
    Delivery Computation ( Depending on the Size of the Products & Location )
    Product Searching
    Product Listing
    Product Description

=== End Front ===


=== Back ( Admin ) ===

    Products
        Add Product
            It automatically Generate Product Variants depending on input
        Edit Product
        Product Listing
        Add Item On Sale

    Inventory
        Suppliers
        Purchase Orders
        Accepting Purchase Order by Quantity

    Orders
        Create Orders
            Discount (Either Peso Value or Percentage)
            Create Order for Customer
        Orders Made by the Customers
        Order Details
            Update Order Status
            Update Order Details (Quantity and etc.)

    Discounts
        Either Peso Value or Percentage
        Either Limited User or Limited Time

    Customer
        Generate Number of Orders
        Generate Total Spending
        Generate Average Spending
        Displays all the Order Made
        Edit User Details

    Reports
        Sales ( Yearly / Monthly / Weekly / Daily )
        Visitor
            Top Refferer
            By Location
            By Country
            By Device
        Stock In Log
            Displays all the Receiving of Purchase Order

    Settings
        Shipping
        Courier

=== End Back ===


=== User Accounts ===

    Admin Account
        Email       : carlo@ezshop.com
        Password    : password
