<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
	}

	public function index() {
		if ($this->session->userdata('customer') === null)
			redirect(base_url());

		$data['page_title'] = "Hair Geek";
		$customer = $this->session->userdata('customer');
		$data['orders'] = $this->Order_Model->get_all_customer_order_all($customer['customer_id']);
        $this->load->view('main/customer/customer_profile_view', $data);
    }

	public function signup() {

		$this->form_validation->set_rules('txt_fname', 'first name', 'required|xss_clean');
		$this->form_validation->set_rules('txt_lname', 'last name', 'required|xss_clean');
		$this->form_validation->set_rules('txt_email', 'email', 'required|xss_clean|valid_email|is_unique[cs_customers.customer_email]');
		$this->form_validation->set_rules('txt_password', 'password', 'required|xss_clean');
		$this->form_validation->set_rules('txt_re_password', 're password', 'required|xss_clean|matches[txt_password]');

		if($this->form_validation->run()) {
			$customer_data = array(
				"customer_fname" => $this->input->post('txt_fname'),
				"customer_lname" => $this->input->post('txt_lname'),
				"customer_email" => $this->input->post('txt_email'),
				"customer_password" => md5($this->input->post('txt_password'))
			);

			$customer_id = $this->Customer_Model->insert_customer($customer_data);

			$user_data = array(
				"customer_id" => $customer_id,
				"fullname" => $this->input->post('txt_fname')
			);

			$this->session->set_userdata('customer', $user_data);

			redirect(base_url() . 'customer/');
		}

		$data['page_title'] = "Hair Geek";
		$this->load->view('main/customer/signup_view', $data);
	}

	public function edit() {
		if ($this->session->userdata('customer') === null)
			redirect(base_url());

		$data['alert'] = "";

		$this->form_validation->set_rules('txt_fname', 'fname', 'required|xss_clean');
		$this->form_validation->set_rules('txt_lname', 'lname', 'required|xss_clean');
		$this->form_validation->set_rules('txt_phone', 'phone', 'required|xss_clean');
		$this->form_validation->set_rules('txt_company', 'company', 'xss_clean');
		$this->form_validation->set_rules('txt_address', 'address', 'required|xss_clean');
		$this->form_validation->set_rules('txt_city', 'city', 'required|xss_clean');
		$this->form_validation->set_rules('txt_state', 'state', 'required|xss_clean');
		$this->form_validation->set_rules('txt_zipcode', 'zipcode', 'required|xss_clean');
		$this->form_validation->set_rules('txt_country', 'country', 'required|xss_clean');

		if($this->form_validation->run()) {
			$customer_data = array(
				"customer_fname" => $this->input->post('txt_fname'),
				"customer_lname" => $this->input->post('txt_lname'),
				"customer_phone" => $this->input->post('txt_phone'),
				"customer_company" => $this->input->post('txt_company'),
				"customer_address" => $this->input->post('txt_address'),
				"customer_city" => $this->input->post('txt_city'),
				"customer_state" => $this->input->post('txt_state'),
				"customer_zipcode" => $this->input->post('txt_zipcode'),
				"customer_country" => $this->input->post('txt_country')
			);
			$customer = $this->session->userdata('customer');
			$customer_id = $customer['customer_id'];
			$this->Customer_Model->update_customer($customer_id, $customer_data);

			$data['alert'] = '<div class="success alert" style="padding: 10px;"><center>Customer Details Updated.</center></div>';
		}

		$data['page_title'] = "Hair Geek";
		$customer = $this->session->userdata('customer');
		$data['c'] = $this->Customer_Model->get_cutomer_detail_by_id($customer['customer_id']);
        $this->load->view('main/customer/edit_customer_view', $data);
	}

}
