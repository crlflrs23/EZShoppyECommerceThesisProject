<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
	}

	public function index() {
		$data['page_title'] = "Hair Geek";
        $this->load->view('main/cart/cart_view', $data);
    }

    public function add($pvid) {
        if($this->Product_Model->get_variant_by_id($pvid) !== null) {

			$unq = "";
            if($this->session->userdata('group') == null) {
				$unq = md5(uniqid());
	            $this->session->set_userdata('group', $unq);
			} else {
				$unq = $this->session->userdata('group');
			}

			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$qty = 1;

			if(isset($_REQUEST['txt_qty'])) {
				$qty = $_REQUEST['txt_qty'];
			}

			if(count($this->Cart_Model->check_product_exists($this->ezclient->get_ip(), $unq, $pvid)) >= 1) {
				$cart_info = $this->Cart_Model->check_product_exists($this->ezclient->get_ip(), $unq, $pvid);

				$cart_data = array(
	                "cart_quantity" => intval($cart_info->cart_quantity)+$qty
	            );

				$this->Cart_Model->update_cart($cart_info->cart_id, $cart_data);
			} else {
				$cart_data = array(
	                "cart_group" => $unq,
	                "customer_ip" => $this->ezclient->get_ip(),
					"cart_quantity" => $qty,
	                "product_variant_id" => $pvid,
	                "date_created" => $time
	            );

				$this->Cart_Model->insert_cart($cart_data);
			}

			if($_REQUEST['redirect_to'] == null) {
				redirect(base_url() . 'cart/');
			} else {
				redirect($_REQUEST['redirect_to']);
			}

        }
    }

	public function update123() {
		$this->form_validation->set_rules('cart_id[]', 'Cart Identification', 'required|xss_clean');
		$this->form_validation->set_rules('cart_qty[]', 'Cart Quantity', 'required|xss_clean');

		if ($this->form_validation->run()) {
			foreach ($this->input->post('cart_id') as $key => $value) {
				$qty = $this->input->post('cart_qty');
				$cart_data = array(
					"cart_quantity" => $qty[$key]
				);

				$this->Cart_Model->update_cart($value, $cart_data);
			}
		}
	}

	public function update($cart_id) {
		$ip = $this->ezclient->get_ip();
		$group = $this->session->userdata('group');
		$cart = $this->Cart_Model->get_cart_by_id($cart_id);

		if($cart->cart_group == $group
		&& $cart->customer_ip == $ip) {
			$cart_data = array(
				"cart_quantity" => $this->input->post('cart_qty')
			);
			$this->Cart_Model->update_cart($cart_id, $cart_data);
			echo "OK!";
		}
	}

	public function delete($cart_id) {
		$ip = $this->ezclient->get_ip();
		$group = $this->session->userdata('group');

		$cart = $this->Cart_Model->get_cart_by_id($cart_id);

		if($cart->cart_group == $group
		&& $cart->customer_ip == $ip) {

			$this->Cart_Model->delete_cart($cart_id);
			echo "OK!";
		}
	}

	public function checkout($group) {

		$this->form_validation->set_rules('txt_fname', 'first name', 'required|xss_clean|ucwords');
		$this->form_validation->set_rules('txt_lname', 'last name', 'required|xss_clean|ucwords');
		$this->form_validation->set_rules('txt_email', 'email', 'required|xss_clean|valid_email');
		$this->form_validation->set_rules('txt_phone', 'phone', 'required|xss_clean');
		$this->form_validation->set_rules('txt_password', 'password', 'xss_clean');
		$this->form_validation->set_rules('txt_re_password', 're password', 'xss_clean|matches[password]');
		$this->form_validation->set_rules('txt_company', 'company', 'xss_clean');
		$this->form_validation->set_rules('txt_address', 'address', 'required|xss_clean');
		$this->form_validation->set_rules('txt_city', 'city', 'required|xss_clean');
		$this->form_validation->set_rules('txt_state', 'state', 'required|xss_clean');
		$this->form_validation->set_rules('txt_zipcode', 'zipcode', 'required|xss_clean');
		$this->form_validation->set_rules('txt_country', 'country', 'xss_clean');
		$this->form_validation->set_rules('txt_payment_method', 'payment method', 'required|xss_clean');
		$this->form_validation->set_rules('txt_discount', 'discount', 'xss_clean');
		$this->form_validation->set_rules('txt_total', 'total', 'xss_clean');
		$this->form_validation->set_rules('txt_shipping', 'shipping', 'xss_clean');
		$this->form_validation->set_rules('tac', 'Terms and Condition', 'required|xss_clean');

		if($this->form_validation->run()) {

			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());
			$variant_arr=$discount_arr=$order_arr=$order_discount_arr=array();
			$order_id = "";

			$customer_id = "";
			if (count($this->Customer_Model->get_cutomer_detail_by_email($this->input->post('txt_email'))) >= 1) {
				$customer_id = $this->Customer_Model->get_cutomer_detail_by_email($this->input->post('txt_email'))->customer_id;
			} else {
				$customer_data = array(
					'customer_fname' => $this->input->post('txt_fname'),
					'customer_lname' => $this->input->post('txt_lname'),
					'customer_email' => $this->input->post('txt_email')
				);

				$customer_id = $this->Customer_Model->insert_customer($customer_data);
			}

			if($this->input->post('account_check') !== null) {
				$customer_pass_data = array(
					"customer_password" => md5($this->input->post('txt_password')),
					"customer_phone" => $this->input->post('txt_phone'),
					"customer_company" => $this->input->post('txt_company'),
					"customer_address" => $this->input->post('txt_address'),
					"customer_city" => $this->input->post('txt_city'),
					"customer_state" => $this->input->post('txt_state'),
					"customer_zipcode" => $this->input->post('txt_zipcode'),
					"customer_country" => "Philippines"
				);

				$this->Customer_Model->update_customer($customer_id, $customer_pass_data);
			}

			$total = intval($this->input->post('txt_total'))+intval($this->input->post('txt_shipping'))+intval($this->input->post('txt_additional_payment'));

			if($this->input->post('txt_payment_method') != "PayPal") {
				$order_data = array(
					"customer_id" => $customer_id,
					"order_subtotal" => $this->input->post('txt_total'),
					"order_shipping" => $this->input->post('txt_shipping'),
					"order_additional_payment" => $this->input->post('txt_additional_payment'),
					"order_total" => $total,
					"order_note" => $this->input->post('txt_note'),
					"order_phone" => $this->input->post('txt_phone'),
					"order_company" => $this->input->post('txt_company'),
					"order_address" => $this->input->post('txt_address'),
					"order_city" => $this->input->post('txt_city'),
					"order_state" => $this->input->post('txt_state'),
					"order_zipcode" => $this->input->post('txt_zipcode'),
					"order_country" => "Philippines",
					"order_link" => $group,
					"date_created" => $time,
					"date_modified" => $time
				);

				$order_id = $this->Order_Model->insert_order($order_data);

				array_push($order_arr, $order_data);

				$group = $this->session->userdata('group');
				$content = $this->Cart_Model->get_cart_content($this->ezclient->get_ip(), $group);

				foreach ($content as $key => $value) {
					$content_data = array(
						"order_id" => $order_id,
						"product_variant_id" => $value->product_variant_id,
						"order_content_qty" => $value->cart_quantity
					);

					array_push($variant_arr, $content_data);

					$this->Order_Model->insert_order_content($content_data);
				}

				$discount_percentage = 0;
				if(count($this->Discount_Model->get_discount_by_code($this->input->post('txt_discount'))) >= 1) {
					$discount = $this->Discount_Model->get_discount_by_code($this->input->post('txt_discount'));
					$discount_data = array(
						"order_id" => $order_id,
						"order_discount_to" => "all",
						"order_discount_value" => $discount->discount_value,
						"product_variant_id" => 0,
						"order_discount_type" => $discount->discount_type,
						"order_discount_reason" => $discount->discount_name
					);

					if($discount->discount_type == 2) {
						$discount_percentage = $discount->discount_value;
					} else {
						$discount_percentage = (intval($discount->discount_value)/intval($total))*100;
					}

					$discount_id = $this->Order_Model->insert_order_discount($discount_data);

					$update_discount_data = array(
						"discount_used" => intval($discount->discount_used) + 1
					);

					$this->Discount_Model->update_discount_by_code($discount->discount_code, $update_discount_data);
					array_push($order_discount_arr, $discount_data);
				}

				$order_meta_data = array(
					"order_id" => $order_id,
					"keyword" => "payment_method",
					"value" => $this->input->post('txt_payment_method')
				);
				$this->Order_Model->insert_meta($order_meta_data);

				$this->Cart_Model->update_cart_by_group($group, array("cart_status" => '1'));
				$this->session->unset_userdata('group');

				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' => $this->Option_Model->get_option_by_value('emailer')->option_value, // change it to yours
					'smtp_pass' => $this->Option_Model->get_option_by_value('emailer_password')->option_value, // change it to yours
					'mailtype' => 'html',
					'charset' => 'iso-8859-1',
					'wordwrap' => TRUE
				);

				$mail_data['title'] = "Hairgeek";
				$mail_data['variant'] = $variant_arr;
				$mail_data['discount'] = $discount_arr;
				$mail_data['order'] = $order_arr;
				$mail_data['order_discount'] = $order_discount_arr;
				$mail_data['computation'] = array(
					"subtotal" => $this->input->post('txt_total'),
					"shipping" => $this->input->post('txt_shipping'),
					"additional" => $this->input->post('txt_additional_payment'),
					"total" => $total
				);

				$order_history_data = array(
					"order_id" => $order_id,
					"order_history_message" => "Order has been Placed",
					"date_created" => $time
				);

				$this->Order_Model->insert_order_history($order_history_data);

				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->from('no-reply@hairgeek.ph', 'HairGeek'); // change it to yours
				$this->email->to($this->input->post('txt_email'));// change it to yours
				$this->email->subject('Order Confirmation for #' . $order_id);
				$this->email->message($this->load->view('email/order_confirmation', $mail_data, TRUE));
				$this->email->send();

				redirect(base_url() . "cart/fulfill/" . $group);
			} else {

				$session_data = array();

				$session_data["customer"] = array(
					'customer_id' => $customer_id,
					'customer_fname' => $this->input->post('txt_fname'),
					'customer_lname' => $this->input->post('txt_lname'),
					'customer_email' => $this->input->post('txt_email')
				);

				$session_data["order"] = array(
					"customer_id" => $customer_id,
					"order_subtotal" => $this->input->post('txt_total'),
					"order_shipping" => $this->input->post('txt_shipping'),
					"order_additional_payment" => $this->input->post('txt_additional_payment'),
					"order_total" => $total,
					"order_note" => $this->input->post('txt_note'),
					"order_phone" => $this->input->post('txt_phone'),
					"order_company" => $this->input->post('txt_company'),
					"order_address" => $this->input->post('txt_address'),
					"order_city" => $this->input->post('txt_city'),
					"order_state" => $this->input->post('txt_state'),
					"order_zipcode" => $this->input->post('txt_zipcode'),
					"order_country" => "Philippines",
					"order_link" => $group,
					"date_created" => $time,
					"date_modified" => $time
				);

				$session_data["discount"] = array(
					"discount_code" => $this->input->post('txt_discount')
				);

				$this->session->set_userdata("order", $session_data);

				redirect(base_url() . 'cart/paypal/' . $group);
			}

		}

		$data['page_title'] = "Hair Geek";
		$data['group'] = $group;
		$customer = $this->session->userdata('customer');
		$data['c'] = $this->Customer_Model->get_cutomer_detail_by_id($customer['customer_id']);
		$this->load->view('main/cart/checkout_view', $data);
	}

	public function paypal($group) {
		if($this->session->userdata("order") !== null) {
			$data = $this->session->userdata("order");
			#print_r($this->session->userdata("order"));

			$content = $this->Cart_Model->get_cart_content($this->ezclient->get_ip(), $group);
			$total = 0;
			foreach ($content as $key => $value) {
				$row = $this->Product_Model->get_variant_by_id($value->product_variant_id);
				$total += $row->product_variant_price*$value->cart_quantity;
			}


			$config['business'] 			= 'carlo29092-facilitator@gmail.com';
			$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
			$config['return'] 				= base_url() . 'cart/process/'. $group .'/?success=true';
			$config['cancel_return'] 		= base_url() . 'cart/process/'. $group .'/?success=false';
			$config['notify_url'] 			= 'process_payment.php'; //IPN Post
			$config['production'] 			= FALSE; //Its false by default and will use sandbox

			if(count($this->Discount_Model->get_discount_by_code($data['discount']['discount_code']))) {
				$discount = $this->Discount_Model->get_discount_by_code($data['discount']['discount_code']);
				if($discount->discount_type == 1) {
					$config['discount_amount_cart'] = $discount->discount_value;
				} else {
					$config['discount_amount_cart'] 	= (intval($total)*(intval($discount->discount_value)/100));
				}
			}

			$config["invoice"]			= uniqid();

			$this->load->library('paypal',$config);


			foreach ($content as $key => $value) {
				$row = $this->Product_Model->get_variant_by_id($value->product_variant_id);
				$product_data = $this->Product_Model->get_product_by_id($row->product_id);

				// Product Name , Price, Quantity, SKU
				$this->paypal->add($product_data->product_title,$row->product_variant_price, $value->cart_quantity, $row->product_variant_sku);
			}

			$this->paypal->add("Shipping Fee", $data['order']['order_shipping']);

			$this->paypal->pay(); //Proccess the payment
		}
	}

	public function process($group) {

		if(isset($_REQUEST['success'])
		&& (bool)$_REQUEST['success']) {

			$data = $this->session->userdata("order");
			$order_data = array(
				"customer_id" => $data['customer']['customer_id'],
				"order_subtotal" => $data['order']['order_subtotal'],
				"order_shipping" => $data['order']['order_shipping'],
				"order_additional_payment" => $data['order']['order_additional_payment'],
				"order_total" =>  $data['order']['order_total'],
				"order_note" =>  $data['order']['order_note'],
				"order_phone" => $data['order']['order_phone'],
				"order_company" => $data['order']['order_company'],
				"order_address" => $data['order']['order_address'],
				"order_city" =>  $data['order']['order_city'],
				"order_state" => $data['order']['order_state'],
				"order_zipcode" => $data['order']['order_zipcode'],
				"order_country" => $data['order']['order_country'],
				"order_link" => $data['order']['order_link'],
				"date_created" => $data['order']['date_created'],
				"date_modified" =>  $data['order']['date_modified'],
				"order_payment_status" => 2
			);

			$order_id = $this->Order_Model->insert_order($order_data);
			$content = $this->Cart_Model->get_cart_content($this->ezclient->get_ip(), $group);

			foreach ($content as $key => $value) {
				$content_data = array(
					"order_id" => $order_id,
					"product_variant_id" => $value->product_variant_id,
					"order_content_qty" => $value->cart_quantity
				);

				$this->Order_Model->insert_order_content($content_data);
			}

			$discount_percentage = 0;
			if(count($this->Discount_Model->get_discount_by_code($data['discount']['discount_code'])) >= 1) {
				$discount = $this->Discount_Model->get_discount_by_code($data['discount']['discount_code']);
				$discount_data = array(
					"order_id" => $order_id,
					"order_discount_to" => "all",
					"order_discount_value" => $discount->discount_value,
					"product_variant_id" => 0,
					"order_discount_type" => $discount->discount_type,
					"order_discount_reason" => $discount->discount_name
				);

				if($discount->discount_type == 2) {
					$discount_percentage = $discount->discount_value;
				} else {
					$discount_percentage = (intval($discount->discount_value)/intval($total))*100;
				}

				$discount_id = $this->Order_Model->insert_order_discount($discount_data);

				$update_discount_data = array(
					"discount_used" => intval($discount->discount_used) + 1
				);

				$this->Discount_Model->update_discount_by_code($discount->discount_code, $update_discount_data);
			}

			$order_meta_data = array(
				"order_id" => $order_id,
				"keyword" => "payment_method",
				"value" => "Paypal"
			);
			$this->Order_Model->insert_meta($order_meta_data);

			$this->Cart_Model->update_cart_by_group($group, array("cart_status" => '1'));

			$content = $this->Cart_Model->get_cart_content($this->ezclient->get_ip(), $group);
			$this->session->unset_userdata('group');

			$order_meta_data = array(
				"order_id" => $order_id,
				"keyword" => "payment_method",
				"value" => "PayPal"
			);
			$this->Order_Model->insert_meta($order_meta_data);


			$order_history_data = array(
				"order_id" => $order_id,
				"order_history_message" => "Order has been Placed & Paid by PayPal",
				"date_created" => $time
			);

			$this->Order_Model->insert_order_history($order_history_data);
		}
		$received_post = print_r($this->input->post(),TRUE);
		redirect(base_url() . "cart/fulfill/" . $group);
	}

	public function fulfill($group) {
		if(count($this->Order_Model->get_order_by_group($group)) < 1) {
			$data["heading"] = "404 Error";
			$data["message"] = "This page doesn't exist. <a href='".base_url()."'>Click here to Shop</a>";
			$this->load->view('errors/html/error_404.php', $data);
		} else {
			$data['page_title'] = "Hair Geek";
			$data['group'] = $group;
			$this->load->view('main/cart/fulfill_view', $data);
		}
	}

	public function confirm($order_id, $group) {
		$this->form_validation->set_rules('txt_note', 'Note', 'xss_clean');
		$this->form_validation->set_rules('files_txt', 'Files', 'required|xss_clean');

		if($this->form_validation->run()) {

			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$proof_data = array(
				"order_id" => $order_id,
				"order_proof_file" => $this->input->post('files_txt'),
				"order_proof_note" => $this->input->post('txt_note'),
				"date_created" => $time
			);

			$order_data = array(
				"order_payment_status" => '1',
				"date_modified" => $time
			);

			$this->Order_Model->update_order($order_data, $order_id);
			$this->Order_Model->insert_proof($proof_data);

			$order_history_data = array(
				"order_id" => $order_id,
				"order_history_message" => "Customer sent a Payment Confirmation.",
				"date_created" => $time
			);

			$this->Order_Model->insert_order_history($order_history_data);

			redirect(base_url() . 'cart/fulfill/'. $group .'/?action=' . md5('consuccess'));
		}

		$data['page_title'] = "Hair Geek";
		$data['group'] = $group;
		$data['order_id'] = $order_id;
		$this->load->view('main/cart/confirm_payment_view', $data);
	}

	public function cancel($order_id, $group) {
		if(count($this->Order_Model->get_order_by_group($group)) < 1) {
			$data["heading"] = "404 Error";
			$data["message"] = "This page doesn't exist. <a href='".base_url()."'>Click here to Shop</a>";
			$this->load->view('errors/html/error_404.php', $data);
		} else {
			$data = array(
				"order_status" => '-1'
			);

			$this->Order_Model->update_order($data, $order_id);

			$order_history_data = array(
				"order_id" => $order_id,
				"order_history_message" => "Customer Cancelled the Order.",
				"date_created" => $time
			);

			$this->Order_Model->insert_order_history($order_history_data);


			redirect(base_url() . 'customer/?action=' . md5('cancel') . '&id='. $order_id);
		}
	}

	public function getSize($local, $size) {
		echo json_encode($this->Shipping_Model->get_shipping_size($local, $size));
	}

	public function getDiscount($code) {
		if(count($this->Discount_Model->get_discount_by_code($code)) >= 1) {
			$disc = $this->Discount_Model->get_discount_by_code($code);
			if($disc->discount_limit != $disc->discount_used) {
				echo json_encode($this->Discount_Model->get_discount_by_code($code));
			} else {
				echo 'null';
			}
		} else {
			echo 'null';
		}
	}

	public function embed() {
		if($this->session->userdata('group') == null) {
			$unq = md5(uniqid());
			$this->session->set_userdata('group', $unq);
		} else {
			$unq = $this->session->userdata('group');
		}

		redirect(base_url() . 'cart/checkout/' . $unq . '/');
	}

}

function check($data) {
	return isset($data) ? $data : '';
}
