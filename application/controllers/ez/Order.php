<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	function __construct(){
	parent::__construct();
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
		$this->load->Model('Order_Model');
		$this->load->Model('Settings_Model');
		date_default_timezone_set('Asia/Manila');
	}

	public function index() {
        $store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;
		$data['orders'] = $this->Order_Model->get_all_orders();
		$this->load->view('orders/all_orders_view', $data);
	}

    public function create() {
		$admin_id = $this->session->userdata('admin');
		$account = $this->Account_Model->get_account_by_id($admin_id);
		$adm_fullname = $account->account_fname . ' ' . $account->account_lname;

		$this->form_validation->set_rules('variants_added', ' Products', 'required|xss_clean');
		$this->form_validation->set_rules('txt_customer_email', ' Email', 'required|valid_email|xss_clean');
		$this->form_validation->set_rules('customer_company', ' Company', 'required|xss_clean');
		$this->form_validation->set_rules('customer_phone', ' Phone', 'required|xss_clean');
		$this->form_validation->set_rules('customer_address', ' Address', 'required|xss_clean');
		$this->form_validation->set_rules('customer_city', ' City', 'required|xss_clean');
		$this->form_validation->set_rules('customer_zipcode', ' Postal/Zipcode', 'required|xss_clean');
		$this->form_validation->set_rules('txt_order_note', ' Order Note', 'xss_clean');

		if($this->form_validation->run()) {
			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());
			$variant_arr=$discount_arr=$order_arr=$order_discount_arr=array();

			$customer_id = $this->Customer_Model->get_cutomer_detail_by_email($this->input->post('txt_customer_email'))->customer_id;

			$order_data = array(
				"customer_id" => $customer_id,
				"order_total" => $this->input->post('txt_order_total'),
				"order_note" => $this->input->post('txt_order_note'),
				"order_company" => $this->input->post('customer_company'),
				"order_phone" => $this->input->post('customer_phone'),
				"order_address" => $this->input->post('customer_address'),
				"order_city" => $this->input->post('customer_city'),
				"order_zipcode" => $this->input->post('customer_zipcode'),
				"date_created" => $time,
				"date_modified" => $time,
				"order_status" => 0
			);

			array_push($order_arr, $order_data);
			$order_id = $this->Order_Model->insert_order($order_data);

			if($this->input->post('discount_value') != "") {
				$order_discount_data = array(
					"order_id" => $order_id,
					"product_variant_id" => 0,
					"order_discount_to" => 'all',
					"order_discount_value" => $this->input->post('discount_value'),
					"order_discount_type" => $this->input->post('discount_value_type'),
					"order_discount_reason" => $this->input->post('txt_discount_reason')
				);
				array_push($order_discount_arr, $order_discount_data);
				$this->Order_Model->insert_order_discount($order_discount_data);
			}

			$vid = explode(',', $this->input->post('variants_added'));
			$p_var = array();



			foreach ($vid as $key => $id) {
				$discount_id = 0;

				if($this->input->post('discount_value_'. $id) !== null
				|| $this->input->post('discount_value_'. $id) != "") {
					$order_item_discount = array(
						"order_id" => $order_id,
						"order_discount_to" => 'item',
						"product_variant_id" => $id,
						"order_discount_value" => $this->input->post('discount_value_'. $id),
						"order_discount_type" => $this->input->post('discount_value_'. $id . '_type'),
						"order_discount_reason" => $this->input->post('discount_value_'. $id . '_reason')
					);
					array_push($discount_arr, $order_item_discount);
					$discount_id = $this->Order_Model->insert_order_discount($order_item_discount);
				}

				$order_content_data = array(
					"order_id" => $order_id,
					"product_variant_id" => $id,
					"order_content_qty" => $this->input->post('qty_'. $id),
					"order_content_discount" => $discount_id
				);

				array_push($variant_arr, $order_content_data);

				$this->Order_Model->insert_order_content($order_content_data);
			}

			$order_history_data = array(
				"order_id" => $order_id,
				"order_history_message" => "Order has been Placed by " . $adm_fullname,
				"date_created" => $time
			);

			$this->Order_Model->insert_order_history($order_history_data);

			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' => $this->Option_Model->get_option_by_value('emailer')->option_value, // change it to yours
				'smtp_pass' => $this->Option_Model->get_option_by_value('emailer_password')->option_value, // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			$data['title'] = "Hairgeek";
			$data['variant'] = $variant_arr;
			$data['discount'] = $discount_arr;
			$data['order'] = $order_arr;
			$data['order_discount'] = $order_discount_arr;

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from('no-reply@hairgeek.ph', 'HairGeek'); // change it to yours
			$this->email->to($this->input->post('txt_customer_email'));// change it to yours
			$this->email->subject('Order Confirmation for #' . $order_id);
			$this->email->message($this->load->view('email/order_confirmation', $data, TRUE));


			$this->email->send();
			#$this->load->view('alert_noproduct_email_view', $data);
			redirect(base_url() . 'ez/order/edit/'. $order_id .'/');
		}

		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;
		$data['customers'] = $this->Customer_Model->gel_all_customers_by_status('1');
        $this->load->view('orders/add_order_view', $data);
    }

	function edit($order_id) {
		$admin_id = $this->session->userdata('admin');
		$account = $this->Account_Model->get_account_by_id($admin_id);
		$adm_fullname = $account->account_fname . ' ' . $account->account_lname;

		$this->form_validation->set_rules('variants_added', ' Products', 'required|xss_clean');
		$this->form_validation->set_rules('txt_customer_email', ' Email', 'required|valid_email|xss_clean');
		$this->form_validation->set_rules('customer_company', ' Company', 'required|xss_clean');
		$this->form_validation->set_rules('customer_phone', ' Phone', 'required|xss_clean');
		$this->form_validation->set_rules('customer_address', ' Address', 'required|xss_clean');
		$this->form_validation->set_rules('customer_city', ' City', 'required|xss_clean');
		$this->form_validation->set_rules('customer_zipcode', ' Postal/Zipcode', 'required|xss_clean');
		$this->form_validation->set_rules('txt_order_note', ' Order Note', 'xss_clean');

		if($this->form_validation->run()) {
			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$customer_id = $this->Customer_Model->get_cutomer_detail_by_email($this->input->post('txt_customer_email'))->customer_id;

			$order_data = array(
				"customer_id" => $customer_id,
				"order_total" => $this->input->post('txt_order_total'),
				"order_note" => $this->input->post('txt_order_note'),
				"order_company" => $this->input->post('customer_company'),
				"order_phone" => $this->input->post('customer_phone'),
				"order_address" => $this->input->post('customer_address'),
				"order_city" => $this->input->post('customer_city'),
				"order_zipcode" => $this->input->post('customer_zipcode'),
				"date_modified" => $time
			);

			$this->Order_Model->update_order($order_data, $order_id);

			$vid = explode(',', $this->input->post('variants_added'));
			$p_var = array();

			$this->Order_Model->delete_order_discount($order_id, 'item');
			$this->Order_Model->delete_order_content($order_id);

			foreach ($vid as $key => $id) {
				$discount_id = 0;

				if($this->input->post('discount_value_'. $id) !== null) {
					$order_item_discount = array(
						"order_id" => $order_id,
						"order_discount_to" => 'item',
						"product_variant_id" => $id,
						"order_discount_value" => $this->input->post('discount_value_'. $id),
						"order_discount_type" => $this->input->post('discount_value_'. $id . '_type'),
						"order_discount_reason" => $this->input->post('discount_value_'. $id . '_reason')
					);

					$discount_id = $this->Order_Model->insert_order_discount($order_item_discount);
				}

				$order_content_data = array(
					"order_id" => $order_id,
					"product_variant_id" => $id,
					"order_content_qty" => $this->input->post('qty_'. $id),
					"order_content_discount" => $discount_id
				);

				$this->Order_Model->insert_order_content($order_content_data);
			}

			$order_history_data = array(
				"order_id" => $order_id,
				"order_history_message" => "Order has been Editted by " . $adm_fullname,
				"date_created" => $time
			);

			$this->Order_Model->insert_order_history($order_history_data);

			redirect(base_url() . 'ez/order/');
		}


		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;
		$data['customers'] = $this->Customer_Model->gel_all_customers_by_status('1');
		$data['order_id'] = $order_id;
		$data['order'] = $this->Order_Model->get_order_by_id($order_id);
		$this->load->view('orders/edit_order_view', $data);
	}

	function fulfillment($order_id) {
		$admin_id = $this->session->userdata('admin');
		$account = $this->Account_Model->get_account_by_id($admin_id);
		$adm_fullname = $account->account_fname . ' ' . $account->account_lname;

		$this->form_validation->set_rules('variants_added', ' Products', 'required|xss_clean');
		$this->form_validation->set_rules('txt_tracking_number', ' Tracking Number', 'required|xss_clean');
		$this->form_validation->set_rules('txt_tracking_courier', ' Courier', 'required|xss_clean');

		if($this->form_validation->run()) {
			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$vid = explode(',', $this->input->post('variants_added'));
			$p_var = array();

			$data = array(
				"order_status" => '1'
			);

			$this->Order_Model->update_order($data, $order_id);

			$fulfillment_data = array(
				"order_id" => $order_id,
				"order_fulfillment_tracking" => $this->input->post('txt_tracking_number'),
				"order_fulfillment_courier" => $this->input->post('txt_tracking_courier'),
				"date_created" => $time
			);

			$this->Order_Model->insert_order_fulfillment($fulfillment_data);

			foreach ($vid as $key => $id) {
				$v_data = $this->Product_Model->get_variant_by_id($id);
				$inven_data = array(
					"inventory_stocks" => intval($this->Inventory_Model->get_inventory_details_sku($v_data->product_variant_sku)->inventory_stocks)-intval($this->input->post('qty_' . $id))
				);

				$this->Inventory_Model->update_stocks_by_sku($inven_data, $v_data->product_variant_sku, $v_data->product_id);
			}

			$order_history_data = array(
				"order_id" => $order_id,
				"order_history_message" => "The Order has been fulfilled by " . $adm_fullname,
				"date_created" => $time
			);

			$this->Order_Model->insert_order_history($order_history_data);

			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' => $this->Option_Model->get_option_by_value('emailer')->option_value, // change it to yours
				'smtp_pass' => $this->Option_Model->get_option_by_value('emailer_password')->option_value, // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			$customer_id = $this->Order_Model->get_order_by_id($order_id)->customer_id;

			$email_data = array(
				"date" => $time,
				"customer_id" => $customer_id,
				"Message" => "The order has been shipped with the tracking number of " . $this->input->post('txt_tracking_number') . " the courier is " . $this->input->post('txt_tracking_courier')
			);

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from('no-reply@hairgeek.ph', 'HairGeek'); // change it to yours
			$this->email->to($this->Customer_Model->get_cutomer_detail_by_id($customer_id)->customer_email); // change it to yours
			$this->email->subject('Order Fulfilled for #' . $order_id);
			$this->email->message($this->load->view('email/order_emailer', $email_data, TRUE));
			$this->email->send();

			redirect(base_url() . 'ez/order/');
		}

		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;
		$data['customers'] = $this->Customer_Model->gel_all_customers_by_status('1');
		$data['order_id'] = $order_id;
		$data['order'] = $this->Order_Model->get_order_by_id($order_id);
		$this->load->view('orders/add_fulfillment_view', $data);
	}

	public function cancelled() {
		$admin_id = $this->session->userdata('admin');
		$account = $this->Account_Model->get_account_by_id($admin_id);
		$adm_fullname = $account->account_fname . ' ' . $account->account_lname;

		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;
		$data['orders'] = $this->Order_Model->get_all_cancelled_orders();
		$this->load->view('orders/all_orders_view', $data);
	}

	function marker($order_id, $action) {
		$admin_id = $this->session->userdata('admin');
		$account = $this->Account_Model->get_account_by_id($admin_id);
		$adm_fullname = $account->account_fname . ' ' . $account->account_lname;

		switch ($action) {
			case 'paid':
					$date = "%Y-%m-%d %H:%i:%s";
					$time = mdate($date, time());

					$data = array(
						"order_payment_status" => '2',
						"date_modified" => $time
					);
					$this->Order_Model->update_order($data, $order_id);

					$order_history_data = array(
						"order_id" => $order_id,
						"order_history_message" => "Hair Geek Confirms Payment by " . $adm_fullname,
						"date_created" => $time
					);

					$this->Order_Model->insert_order_history($order_history_data);

					$config = Array(
						'protocol' => 'smtp',
						'smtp_host' => 'ssl://smtp.googlemail.com',
						'smtp_port' => 465,
						'smtp_user' => $this->Option_Model->get_option_by_value('emailer')->option_value, // change it to yours
						'smtp_pass' => $this->Option_Model->get_option_by_value('emailer_password')->option_value, // change it to yours
						'mailtype' => 'html',
						'charset' => 'iso-8859-1',
						'wordwrap' => TRUE
					);

					$customer_id = $this->Order_Model->get_order_by_id($order_id)->customer_id;

					$email_data = array(
						"date" => $time,
						"customer_id" => $customer_id,
						"Message" => "We've already Receive your payment. Please wait for the shipping details. Thank you!"
					);

					$this->load->library('email', $config);
					$this->email->set_newline("\r\n");
					$this->email->from('no-reply@hairgeek.ph', 'HairGeek'); // change it to yours
					$this->email->to($this->Customer_Model->get_cutomer_detail_by_id($customer_id)->customer_email); // change it to yours
					$this->email->subject('Payment Accepted for #' . $order_id);
					$this->email->message($this->load->view('email/order_emailer', $email_data, TRUE));
					$this->email->send();


					redirect(base_url() . 'ez/order/edit/'. $order_id .'/');

				break;

				case 'reject':
						$date = "%Y-%m-%d %H:%i:%s";
						$time = mdate($date, time());

						$data = array(
							"order_payment_status" => '3',
							"date_modified" => $time
						);
						$this->Order_Model->update_order($data, $order_id);

						$order_history_data = array(
							"order_id" => $order_id,
							"order_history_message" => "Hair Geek Request Another Proof Payment by " . $adm_fullname,
							"date_created" => $time
						);

						$this->Order_Model->insert_order_history($order_history_data);

						$config = Array(
							'protocol' => 'smtp',
							'smtp_host' => 'ssl://smtp.googlemail.com',
							'smtp_port' => 465,
							'smtp_user' => $this->Option_Model->get_option_by_value('emailer')->option_value, // change it to yours
							'smtp_pass' => $this->Option_Model->get_option_by_value('emailer_password')->option_value, // change it to yours
							'mailtype' => 'html',
							'charset' => 'iso-8859-1',
							'wordwrap' => TRUE
						);

						$customer_id = $this->Order_Model->get_order_by_id($order_id)->customer_id;

						$email_data = array(
							"date" => $time,
							"customer_id" => $customer_id,
							"Message" => "We would like to request another proof of payment, you might have uploaded a wrong image."
						);

						$this->load->library('email', $config);
						$this->email->set_newline("\r\n");
						$this->email->from('no-reply@hairgeek.ph', 'HairGeek'); // change it to yours
						$this->email->to($this->Customer_Model->get_cutomer_detail_by_id($customer_id)->customer_email); // change it to yours
						$this->email->subject('Request a Proof Payment for #' . $order_id);
						$this->email->message($this->load->view('email/order_emailer', $email_data, TRUE));
						$this->email->send();


						redirect(base_url() . 'ez/order/edit/'. $order_id .'/');

					break;
			case 'pending':
				$data = array(
					"order_payment_status" => '0'
				);
				$this->Order_Model->update_order($data, $order_id);
				echo 'OK!';
				break;
			default:
				# code...
				break;
		}
	}

	public function play() {
		$email_data = array(
			"date" => "12/12/12",
			"customer_id" => '1',
			"Message" => "The order has been shipped with the tracking number of 123123123 the courier is LBC"
		);
		$this->load->view('email/order_emailer', $email_data);

	}
}

function ago($time) {
   $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
   $lengths = array("60","60","24","7","4.35","12","10");

   $now = time();

       $difference     = $now - $time;
       $tense         = "ago";

   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
       $difference /= $lengths[$j];
   }

   $difference = round($difference);

   if($difference != 1) {
       $periods[$j].= "s";
   }

   if($j >= 3) {
	   $date = "%Y-%m-%d %H:%i:%s";
 	   return mdate($date, $time);
   } else {
	    return "$difference $periods[$j] ago ";
   }
}
