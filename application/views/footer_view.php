<?php
    if($this->session->userdata('admin') === null) {
        redirect(base_url() . 'ez/');
    }
 ?>

 <script type="text/javascript">
     $(function() {
         $("input[type='number']").attr('min', '1');
         $("input[type='date']").attr('min', '<?=date('Y-m-d', time())?>');
     });
 </script>

<footer>
    <div class="row"><Br/>
        All Rights Reserved EZ Solution<Br/><Br/>
    </div>
</footer>
