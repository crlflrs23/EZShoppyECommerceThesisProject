<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    .upload-holder {
        width: 100%;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Items on Sale</h3><a href="<?=base_url(). 'ez/product/sale/list/'?>"><span class="btn-more" style="color:#fff;">Add Item</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row">

                </div>
                <div class="row field">
                    <div class="centered push_one eight columns">
                        <center><h2>Item on Sale</h2>
                        <p>List of all items on sale</p></center>
                        <Br/>
                        <form method="post" class="append">
                            <span class="txt-label">Product Name</span><br/>
                            <input type="text" class="input" name="search_temp" placehoder="Search Product" list="search-datalist">
                            <datalist id="search-datalist">
                                <?php foreach($products as $row): ?>
                                    <option data-value="<?=$row->product_id?>" value="<?=ucwords(strtolower($row->product_title))?>" />
                                <?php endforeach; ?>
                            </datalist>
                            <div class="medium primary btn"><i class="icon-search" style="color: #fff;"></i></div>
                        </form>
                    </div>

                    <div class="centered push_one twelve columns">
                        <Br/>

                        <table class="paginate" max="10">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th></th>
                                    <th><center><span class="product-title">Product Variant</span></center></th>
                                    <th><center><span class="product-title">SKU</span></center></th>
                                    <th><center><span class="product-title">Quantity</span></center></th>
                                    <th><center><span class="product-title">Days Remaining</span></center></th>
                                    <th><center><span class="product-title">Action</span></center></th>
                                </tr>
                            </thead>
                            <?php foreach ($this->Reports_Model->get_all_product_on_sale() as $key => $value): ?>
                                <?php
                                    $vi = $this->Product_Model->get_variant_by_id($value->product_variant_id);
                                    $pi = $this->Product_Model->get_product_by_id($vi->product_id);

                                    $img = $vi->product_variant_img;

                                    if(strpos($img, ',')) {
                                        $imgs = explode(',', $img);
                                        $img = $imgs[0];
                                    }

                                    $date_reamains = (strtotime($value->product_sale_expires) - time())/60/60/24;
                                    $date_reamains = abs(number_format($date_reamains, 0));

                                 ?>
                            <tr class="stocks-row <?=$vi->product_variant_id?>_holder"  data-tag="<?=$pi->product_title?>">
                                <td>
                                    <img src="<?=base_url()?>img/products/<?=$img?>" width="90" alt="">
                                </td>

                                <td>
                                    <span class="product-title"><?=$pi->product_title?></span><Br/>
                                    <span class="txt-label" style="color: #000;"><?=$vi->product_variant_name?></span>
                                </td>

                                <td>
                                    <span class="product-title"><?=$vi->product_variant_sku?></span><Br/>
                                    <span class="txt-label">Sku</span>
                                </td>

                                <td>
                                    <span class="product-title"><?=$this->Reports_Model->get_sold_count($value->product_sale_id)->sold_count?>/<?=$this->Inventory_Model->get_inventory_details_sku($vi->product_variant_sku)->inventory_stocks?></span><Br/>
                                    <span class="txt-label">Sold/Stock</span>
                                </td>

                                <td>
                                    <span class="product-title"><?=$date_reamains?></span><Br/>
                                    <span class="txt-label" >Days Remaining</span>
                                </td>

                                <td class="field">
                                    <a href="<?=base_url()?>ez/product/sale/end/<?=$value->product_sale_id?>/">
										<span class="btn-more" style="margin-right: 5px !important; float: none;">End Sale</span>
									</a>
                                </td>

                            </tr>
                            <?php endforeach; ?>

                        </table>
                    </div>
                </div>
            </div>

        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script>
$(function() {



    $('input[name="search_temp"]').keyup('change', function() {
        var data = $('#search-datalist'),
            sel = $(this).val();

        $( ".stocks-row[data-tag]" ).each(function( index ) {
            if($(this).attr('data-tag').toLowerCase().indexOf(sel.toLowerCase())) {
                $(this).hide("fast");
            } else {
                $(this).show("fast");
            }
        });

    });

});
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
