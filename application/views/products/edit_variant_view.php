<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .ui-helper-hidden-accessible {
        display: none;
    }

    .img-items {
		border: 1px solid #e7e7e7;
		margin: 2px 2px 15px 2px !important;
        display: inline;
		border-radius: 3px;
		-webkit-box-shadow: 0 4px 4px -2px rgba(0, 0, 0, 0.5);
		-moz-box-shadow: 0 4px 4px -2px rgba(0, 0, 0, 0.5);
		box-shadow: 0 4px 4px -2px rgba(0, 0, 0, 0.5);
	}

    .upload-holder {
        width: 100%;
        padding: 20px;
        height: 50px;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

    input[type='checkbox'] {
        width: 16px;
        height: 16px;
        display: block !important;
        -webkit-appearance: checkbox;
    }
    .progress {
        display: block;
        text-align: center;
        width: 0;
        height: 3px;
        background: red;
        transition: width .3s;
    }
    .progress.hide {
        opacity: 0;
        transition: opacity 1.3s;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Edit Product</h3><a href="<?=base_url(). 'ez/product/manage/variant/' . $variant->product_id?>/"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row field">
                    <div class="push_one fifteen columns">
                        <form action="<?=base_url()?>ez/product/manage/edit/<?=$variant->product_variant_id?>/" method="post">
                            <div class="row">
                                <span class="txt-label">Variant Info</span><Br/>
                                <div class="eight columns prepend">
                                    <span class="txt-label">Variant Price</span><Br/>
                                    <span class="adjoined">₱</span><input type="number" class="input wide" name="txt_variant_price" value="<?=$variant->product_variant_price?>" placeholder="Variant Price">
                                    <Br/><?=form_error("txt_variant_price","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?><br/>
                                </div>

                                <div class="eight columns prepend">
                                    <span class="txt-label">Capital</span><Br/>
                                    <span class="adjoined">₱</span><input type="number" class="input wide" name="txt_variant_capital" value="<?=$variant->product_variant_capital?>" placeholder="Variant Capital">
                                    <Br/><?=form_error("txt_variant_capital","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?><br/>
                                </div>

                                <div class="seven columns prepend">
                                    <span class="txt-label">Variant Size</span><Br/>
                                    <span class="adjoined">Size</span><input type="text" class="input wide" name="txt_variant_weight" value="<?=$variant->product_variant_weight?>" placeholder="Variant Weight">
                                    <Br/><?=form_error("txt_variant_weight","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?><br/>
                                    <span class="txt-label">
                                        Size varies on how many does this product fit on a shipping package.
                                    </span>
                                </div>
                            </div>


                            <div class="row">
                                <span class="txt-label">Inventory Info</span><Br/>
                                <div class="eight columns prepend">
                                    <span class="txt-label">Critical Value</span><Br/>
                                    <span class="adjoined">Pcs</span><input type="number" min="1" class="input wide" name="txt_critical_value" value="<?=$variant->product_variant_critical_value?>" placeholder="Critical Value">
                                    <Br/><?=form_error("txt_critical_value","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?><br/>
                                </div>
                            </div>

                            <div class="row" style="border-top: 1px solid #7997c1; margin-top:30px;">
                                <br/>
                                <div class="progress"></div>
                                <div class="four columns">
                                    <span class="txt-label">Variant Images</span>
                                    <div class="upload-holder medium default btn">
                                        <input type='file' name="file_prod_images" id="inputFile" multiple/>
                                        Select Image
                                    </div>

                                    <input class="input" type="hidden" name="file_prod_images_txt"  value="<?=$variant->product_variant_img?>" />
                                    <?=form_error("file_prod_images_txt","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?>
                                </div>

                                <div class="eleven columns" id="img-loader">
                                    <?php
                                        if($variant->product_variant_img != "") {
                                            $imgs = explode(',', $variant->product_variant_img);
                                            foreach ($imgs as $key => $value) {
                                                echo "<div class='img-items'><img src='". base_url() . "/img/products/" .$value ."' stlye='max-width: 100%;' /></div>";
                                            }
                                        } else {
                                            echo '<center><img src="'.base_url().'img/fil.jpg" alt="" /></center>';
                                        }
                                    ?>
                                </div>
                            </div>

                            <div class="row"><br/><Br/>
                                <div class="centered four columns">
                                    <center><input class="medium primary btn" type="submit" name="btn_prod_add" value="Update Variant" style="color: #fff; font: 700 16px 'Open Sans', sans-serif;"></center>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <br/>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script src="<?=base_url()?>js/custom.js"></script>
<link rel="stylesheet" href="<?=base_url()?>css/tagit.ui-zendesk.css">
<link rel="stylesheet" href="<?=base_url()?>css/jquery.tagit.css">
<script src="<?=base_url()?>js/tinymce.min.js"></script>
<script src="<?=base_url()?>js/tag-it.js"></script>
<script>

    function readURL(input) {
        $("#img-loader").html("");
        if(input.files.length >= 1) {
            for(var i = 0; i < input.files.length; i++) {
                if (input.files && input.files[i]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $("#img-loader").append("<div class='img-items'><img src='"+ e.target.result +"' stlye='max-width: 100%;' /></div>");
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        } else {
            $("#img-loader").append('<center><img src="<?=base_url()?>img/fil.jpg" alt="" /></center>');
        }
    }

    $("#inputFile").change(function() {
        readURL(this);
    });
</script>

<script type="text/javascript">


    $(function() {

        $('form').on('keyup keypress', function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });

        $('input[name="file_prod_images"]').on('change', function() {
            var form_data = new FormData();
            for(var i = 0; i < $(this).prop('files').length; i++) {
                form_data.append('file[]', $(this).prop('files')[i]);
            }

            if($(this).prop('files').length >= 1) {
                $.ajax({
                    xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                            if (percentComplete === 1) {
                                $('.progress').addClass('hide');
                            }
                        }
                    }, false);
                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                        }
                    }, false);
                    return xhr;
                    },
                    url: '<?=base_url()?>ez/product/image/upload/', // point to server-side PHP script
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(response){
                        if(response != "") {
                            $("input[name='file_prod_images_txt']").val(response);
                        }
                    }
                });
            }
        });

    });
</script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.min.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
