<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .ui-helper-hidden-accessible {
        display: none;
    }

    .img-items {
		border: 1px solid #e7e7e7;
		margin: 2px 2px 15px 2px !important;
        display: inline;
		border-radius: 3px;
		-webkit-box-shadow: 0 4px 4px -2px rgba(0, 0, 0, 0.5);
		-moz-box-shadow: 0 4px 4px -2px rgba(0, 0, 0, 0.5);
		box-shadow: 0 4px 4px -2px rgba(0, 0, 0, 0.5);
	}

    .upload-holder {
        width: 100%;
        padding: 20px;
        height: 50px;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

    input[type='checkbox'] {
        width: 16px;
        height: 16px;
        display: block !important;
        -webkit-appearance: checkbox;
    }
    .progress {
        display: block;
        text-align: center;
        width: 0;
        height: 3px;
        background: red;
        transition: width .3s;
    }
    .progress.hide {
        opacity: 0;
        transition: opacity 1.3s;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;">Add Product</h3><a href="<?=base_url(). 'ez/product/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row field">
                    <div class="push_one fifteen columns">
                        <form action="<?=base_url()?>ez/product/add/" method="post">
                            <div class="row">
                                <div class="ten columns">
                                    <span class="txt-label" style="font-size: 12px;">Product Name</span>
                                    <input class="input" type="text" name="txt_prod_name" placeholder="Product Name" value="<?=set_value('txt_prod_name')?>">
                                    <br/><?=form_error("txt_prod_name","<span class='txt-label' style='font-size: 12px;color:red !important;'>","</span>")?><br/>
                                    <span class="txt-label" style="font-size: 12px;">Product Description</span>
                                    <textarea class="input textarea" name="txt_prod_desc" placeholder="Product Description"><?=set_value('txt_prod_desc')?></textarea>
                                    <?=form_error("txt_prod_desc","<span class='txt-label' style='font-size: 12px;color:red !important;'>","</span>")?>
                                </div>
                                <div class="five columns">
                                    <span class="txt-label" style="font-size: 12px;">Product Category</span>
                                    <input class="input" type="text" name="txt_prod_categ_temp" placeholder="Product Category" list="txt_prod_categ_temp_list" value="<?=set_value('txt_prod_categ_temp')?>">
                                    <datalist id="txt_prod_categ_temp_list">
                                        <?php foreach($types as $row): ?>
                                            <option data-value="<?=$row->product_type_id?>" value="<?=$row->product_type_name?>" />
                                        <?php endforeach; ?>
                                    </datalist>
                                    <input type="hidden" name="txt_prod_categ" value="<?=set_value('txt_prod_categ')?>" />
                                    <?=form_error("txt_prod_categ_temp","<span class='txt-label' style='font-size: 12px;color:red !important;'>","</span>")?>
                                    <span class="txt-label txt_prod_categ_temp_error" style="color: red; font-size: 12px;"></span>
                                    <br/><br/>

                                    <span class="txt-label" style="font-size: 12px;">Product Brand</span>
                                    <input class="input" type="text" name="txt_prod_brand_temp" placeholder="Product Brand" list="txt_prod_brand_temp_list" value="<?=set_value('txt_prod_brand_temp')?>">
                                    <datalist id="txt_prod_brand_temp_list">
                                        <?php foreach($brands as $row): ?>
                                            <option data-value="<?=$row->product_brand_id?>" value="<?=$row->product_brand_name?>" />
                                        <?php endforeach; ?>
                                    </datalist>
                                    <input type="hidden" name="txt_prod_brand" value="<?=set_value('txt_prod_brand')?>"/>
                                    <?=form_error("txt_prod_brand_temp","<span class='txt-label' style='font-size: 12px;color:red !important;'>","</span>")?>
                                    <span class="txt-label txt_prod_brand_temp_error" style="color: red; font-size: 12px;"></span>
                                    <br/><br/>

                                    <span class="txt-label" style="font-size: 12px;">Product Tags</span>
                                    <input class="input" type="text" name="txt_prod_tags" placeholder="Product Tags" value="<?=set_value('txt_prod_tags')?>">
                                    <span class="txt-label txt_prod_tags_error" style="font-size: 12px;">Tags are seperated with comma</span><Br/>
                                    <?=form_error("txt_prod_tags","<span class='txt-label' style='font-size: 12px;color:red !important;'>","</span>")?>
                                    <br/>
                                </div>
                            </div>

                            <div class="row" style="border-top: 1px solid #7997c1; margin-top:30px;">
                                <br/>
                                <div class="progress"></div>
                                <div class="four columns">
                                    <span class="txt-label">Product Images</span>
                                    <div class="upload-holder medium default btn">
                                        <input type='file' name="file_prod_images" id="inputFile" multiple/>
                                        Select Image
                                    </div>

                                    <input class="input" type="hidden" name="file_prod_images_txt" value="<?=set_value('file_prod_images_txt')?>" />
                                    <?=form_error("file_prod_images_txt","<span class='txt-label' style='font-size: 12px;color:red !important;'>","</span>")?>
                                </div>

                                <div class="eleven columns" id="img-loader">
                                    <?php
                                        if(set_value('file_prod_images_txt') != "") {
                                            $imgs = explode(',', set_value('file_prod_images_txt'));
                                            foreach ($imgs as $key => $value) {
                                                echo "<div class='img-items'><img src='". base_url() . "/img/products/" .$value ."' stlye='max-width: 100%;' /></div>";
                                            }
                                        } else {
                                            echo '<center><img src="'.base_url().'img/fil.jpg" alt="" /></center>';
                                        }
                                    ?>
                                </div>
                            </div>

                            <div class="row" style="border-top: 1px solid #7997c1; margin-top:30px;">
                                <br/>
                                <div class="eight columns prepend fields">
                                    <span class="txt-label">Pricing</span><br/>
                                    <span class="txt-label" style="font-size: 12px;">Product Price</span><BR/>
                                    <span class="adjoined">₱</span><input class="input wide" type="number" name="txt_prod_price" value="<?=set_value('txt_prod_price')?>"  placeholder="Product Price">
                                    <br/><?=form_error("txt_prod_price","<span class='txt-label' style='font-size: 12px;color:red !important;'>","</span>")?><br/>
                                </div>
                                <!--
                                <div class="seven columns">
                                    <span class="txt-label">Visibility</span><br/><br/>

                                    <table>
                                        <tr>
                                            <td style="width: 15px;"><input type="checkbox" checked name="chk_prod_show_online" value="1"></td>
                                            <td><span class="txt-label" style="color: #000">Online Store</span></td>
                                        </tr>

                                        <tr>
                                            <td style="width: 15px;"><input type="checkbox" name="chk_prod_show_local" value="2"></td>
                                            <td><span class="txt-label" style="color: #000">Local Store</span></td>
                                        </tr>
                                    </table>
                                    <span class="txt-label" style="font-size: 12px;">Publish This product on</span><BR/>
                                    <div class="row">
                                        <div class="eight columns">
                                            <span class="txt-label" style="font-size: 12px;">Date</span><BR/>
                                            <input class="input" type="date" name="date_prod_publish" placeholder="Date" value="<?=set_value('date_prod_publish')?>" >
                                            <br/><br/>
                                        </div>

                                        <div class="eight columns">
                                            <span class="txt-label" style="font-size: 12px;">Time</span><BR/>
                                            <input class="input" type="time" name="time_prod_publish" placeholder="Time" value="<?=set_value('time_prod_publish')?>" >
                                            <br/><br/>
                                        </div>
                                    </div>

                                </div>
                            -->

                            <div class="seven columns">
                                <span class="txt-label">Stocks</span><br/>
                                <div class="row field">
                                    <div class="eight columns">
                                        <span class="txt-label" style="font-size: 12px;">Stock on Hand</span><BR/>
                                        <input class="input" type="number" value="<?=set_value('txt_prod_stock_on_hand')?>" name="txt_prod_stock_on_hand" placeholder="Stock on Hand">
                                        <br/><?=form_error("txt_prod_stock_on_hand","<span class='txt-label' style='font-size: 12px;color:red !important;'>","</span>")?><br/>
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div class="row" style="border-top: 1px solid #7997c1; margin-top:30px;">
                                <br/>
                                <div class="seven columns fields">
                                    <span class="txt-label">Inventory</span><br/>
                                    <div class="row">
                                        <div class="eight columns">
                                            <span class="txt-label" style="font-size: 12px;">SKU (Stock Keeping Unit)</span><BR/>
                                            <input class="input" type="text" value="<?=set_value('txt_prod_sku')?>" name="txt_prod_sku" placeholder="SKU (Stock Keeping Unit)">
                                            <br/><?=form_error("txt_prod_sku","<span class='txt-label' style='font-size: 12px;color:red !important;'>","</span>")?><br/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="border-top: 1px solid #7997c1; margin-top:30px;">
                                <br/>
                                <div class="seven columns prepend field">
                                    <span class="txt-label">Shipping</span><br/>
                                    <div class="row">
                                        <span class="txt-label" style="font-size: 12px;">Size in Units</span><BR/>
                                        <span class="adjoined">Size</span><input class="input wide" type="number" min="0" max="63" name="txt_prod_weight" value="<?=set_value('txt_prod_weight')?>" placeholder="Product Size">
                                        <div class="txt-label" style="font-size: 12px;"><br/>
                                            Size varies how many this product can fit to the shipping package.
                                        </div>
                                        <br/><?=form_error("txt_prod_weight","<span class='txt-label' style='font-size: 12px;color:red !important;'>","</span>")?>
                                    </div>
                                </div>

                                <div class="eight columns">

                                </div>
                            </div>

                            <div class="row" style="border-top: 1px solid #7997c1; margin-top:30px;">
                                <br/>
                                <div class="four columns prepend fields">
                                    <span class="txt-label">Product Variants</span><br/>
                                    <div id="btn_add_variant" class="medium default btn" style="width: 100%;">Add Variants</div>
                                    <input class="input" type="hidden" name="txt_variants" value="<?=set_value('txt_variants')?>">
                                    <div class="txt-label" style="font-size: 12px;"><br/>
                                        Add Variant if this product has different<Br/>color / sizes or event with different prices<br/> on every specifications
                                    </div>
                                </div>

                                <div class="eleven columns">
                                    <div id="variant_loader">
                                        <div class="row">
                                            <div class="four columns"><span class='txt-label'>Variant Type</span></div>
                                            <div class="ten columns"><span class='txt-label'>Values</span></div>
                                            <div class="two columns"><span class='txt-label'>Remove</span></div>

                                            <?php
                                                if(set_value('txt_variants') != "") {
                                                    $variants = explode(',', set_value('txt_variants'));
                                                    foreach ($variants as $key => $value) {
                                                        $count = substr($value, strpos($value, '_')+1);
                                                        echo "
                                                        <div class='row field row_". $count ."'>
                                                            <div class='four columns'>
                                                                <input data-value='variant_name' value='". set_value($value) ."' type='text' class='input' name='variant_". $count ."' list='variant_list' placeholder='Variant Type'/>
                                                                <datalist id='variant_list'>
                                                                    <option value='Size' />
                                                                    <option value='Color' />
                                                                    <option value='Material' />
                                                                </datalist>
                                                            </div>

                                                            <div class='ten columns'>
                                                                <input type='text' class='input' value='". set_value($value. '_tags') ."' name='variant_". $count ."_tags' data-value='variant_tags' placeholder='Variant Tags' />
                                                            </div>

                                                            <div class='two columns'>
                                                                <span class='medium default btn variant_remover' data-value='row_". $count ."'><i class='icon-cancel'></i></span>
                                                            </div>
                                                        </div>";
                                                    }
                                                }
                                            ?>
                                        </div>
                                    </div>

                                    <div style="border-top: 1px solid #7997c1; margin-top:30px;">
                                        <Br/>
                                        <div class="row">
                                            <div class="one columns"></div>
                                            <div class="three columns txt-label" style="font-size: 12px;">Variant Name</div>
                                            <div class="four columns txt-label" style="font-size: 12px;">Price</div>
                                            <div class="four columns txt-label" style="font-size: 12px;">SKU (Stock Keeping Unit)</div>
                                        </div>
                                        <div id="variant_generated"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row"><br/><Br/>
                                <div class="centered four columns">
                                    <center><input class="medium primary btn" type="submit" name="btn_prod_add" value="Add Product" style="color: #fff; font: 700 16px 'Open Sans', sans-serif;"></center>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <br/>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script src="<?=base_url()?>js/custom.js"></script>
<link rel="stylesheet" href="<?=base_url()?>css/tagit.ui-zendesk.css">
<link rel="stylesheet" href="<?=base_url()?>css/jquery.tagit.css">
<script src="<?=base_url()?>js/tinymce.min.js"></script>
<script src="<?=base_url()?>js/tag-it.js"></script>
<script>

    function readURL(input) {
        $("#img-loader").html("");
        if(input.files.length >= 1) {
            for(var i = 0; i < input.files.length; i++) {
                if (input.files && input.files[i]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $("#img-loader").append("<div class='img-items'><img src='"+ e.target.result +"' stlye='max-width: 100%;' /></div>");
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        } else {
            $("#img-loader").append('<center><img src="<?=base_url()?>img/fil.jpg" alt="" /></center>');
        }
    }

    $("#inputFile").change(function() {
        readURL(this);
    });

    $('input[name="txt_prod_tags"]').tagit({
        singleField: true,
        singleFieldNode: $('input[name="txt_prod_tags"]')
    });

    tinymce.init(
        {
            selector:'textarea',
            plugins: [
       "advlist autolink lists link image charmap print preview anchor",
       "searchreplace visualblocks code fullscreen",
       "insertdatetime media table contextmenu paste imagetools"
   ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            menubar: false,
            max_height: 300
        }
    );
</script>

<script type="text/javascript">
    <?php
        if(set_value('txt_variants') != "") {
            $variants = explode(',', set_value('txt_variants'));
            foreach ($variants as $key => $value) {
    ?>
            $('input[name="<?=$value?>_tags"]').tagit({
                singleField: true,
                singleFieldNode: $('input[name="variant_1_tags"]'),
                allowSpaces: true
            });
    <?php
            }
        }
     ?>
</script>

<script type="text/javascript">


    $(function() {
        generate_variants();
        generate_sku();

        if($('input[data-value="variant_name"]').length !== 0) {
            $('input[name="btn_prod_add"]').removeAttr('disabled');
        }

        $('input[name="txt_prod_categ_temp"], input[name="txt_prod_brand_temp"]').on('change', function() {
            if($(this).val() != "") {
                var name = $(this).attr('name'),
                    data = $('#' + name + "_list"),
                    sel = $(this).val(),
                    id = data.find('option[value="'+ sel +'"]').attr('data-value');

                var oName = (name == "txt_prod_categ_temp")? "txt_prod_categ": oName = "txt_prod_brand";

                if(id == null) {
                    $('input[name="'+ oName +'"]').val(sel);
                    $('.' + name + "_error").html(sel + " will automatically be added to the database.");
                } else {
                    $('input[name="'+ oName +'"]').val(id);
                    $('.' + name + "_error").html(" ");
                }
            } else {
                $('.' + name + "_error").html("This Field is important");
            }
        });

        $("#btn_add_variant").click(function() {
            var count = Math.floor((Math.random() * 1000000) + 1);
            $('#variant_loader').append("<div class='row field row_"+ count +"'><div class='four columns'><input data-value='variant_name' type='text' class='input' name='variant_"+ count +"' list='variant_list' placeholder='Variant Type'/><datalist id='variant_list'><option value='Size' /><option value='Color' /><option value='Material' /></datalist></div><div class='ten columns'><input type='text' class='input' name='variant_"+ count +"_tags' data-value='variant_tags' placeholder='Variant Tags' /></div><div class='two columns' ><span class='medium default btn variant_remover' data-value='row_"+ count +"'><i class='icon-cancel'></i></span></div></div>");
            $('input[name="variant_'+ count +'_tags"]').tagit({
                singleField: true,
                singleFieldNode: $('input[name="variant_1_tags"]'),
                allowSpaces: true
            });
            console.log(count);
            if($('input[data-value="variant_tags"]').length == 0) $("#variant_generated").empty();
        });

        $(document).on('click', '.variant_remover', function(){
            var cls = $(this).attr('data-value');
            $('div.' + cls).remove();
            generate_variants();
            if($('input[data-value="variant_tags"]').length == 0) $("#variant_generated").empty();
        });

        $(document).on('change', 'input[data-value="variant_tags"]', function() {
            generate_variants();
            if($('input[data-value="variant_tags"]').length == 0) $("#variant_generated").empty();

        });

        $('input[name="txt_prod_price"], input[name="txt_prod_name"]').on('change', function() {
            generate_variants();
            generate_sku();
        });

        $('input[name="file_prod_images"]').on('change', function() {
            var form_data = new FormData();
            for(var i = 0; i < $(this).prop('files').length; i++) {
                form_data.append('file[]', $(this).prop('files')[i]);
            }


            if($(this).prop('files').length >= 1) {
                $.ajax({
                    xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                            if (percentComplete === 1) {
                                $('.progress').addClass('hide');
                            }
                        }
                    }, false);
                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                        }
                    }, false);
                    return xhr;
                    },
                    url: '<?=base_url()?>ez/product/image/upload/', // point to server-side PHP script
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(response){
                        if(response != "") {
                            $("input[name='file_prod_images_txt']").val(response);
                        }
                    }
                });
            }
        });

        function generate_variants() {
            $("#variant_generated").html(" ");
            var arrays = [],
                field_names = [];
            for(var h = 0; h < $('input[data-value="variant_tags"]').length; h++) {
                arrays.push($('input[data-value="variant_tags"]:eq('+ h +')').val().split(','));
                field_names.push($('input[data-value="variant_name"]:eq('+ h +')').attr('name'));
            }

            $("input[name='txt_variants']").val(field_names.join(','));

            var prod_name = $("input[name='txt_prod_name']").val();

            // Generate SKU
            var prod_initials = prod_name.split(' ').map(function (s) { return s.charAt(0); }).join('');

            if(arrays.length != 0) {
                var arr = allPossibleCases(arrays);

                if(arr.length <= 0 || arr[0] == "") {
                    $("#variant_generated").html(" ");
                } else {
                    for(var i = 0; i < arr.length; i++) {
                        var p_name = arr[i].replace(' ', '_'),
                            p_name_init = p_name.split('_').map(function (s) { return s.charAt(0); }).join('');
                            p_name = p_name.replace(' ', '_');
                        $("#variant_generated").append("<div class='row field'><div class='one columns'><input checked type='checkbox' name='"+ p_name  +"_box' /></div><div class='three columns'> "+ arr[i]  +"</div><div class='four columns'><input class='input' type='text' name='"+ p_name  +"_price' value='"+ $('input[name="txt_prod_price"]').val() +"' /></div><div class='four columns'><input class='input' type='text' name='"+ p_name  +"_sku' value='"+ prod_initials + "" + p_name_init + "" + i +"' /></div></div>");
                    }
                }
            }
        }

        function generate_sku() {
            var prod_name = $("input[name='txt_prod_name']").val();

            // Generate SKU
            var prod_initials = prod_name.split(' ').map(function (s) { return s.charAt(0); }).join('');
            $('input[name="txt_prod_sku"]').attr("value", prod_initials);
        }
    });

    function allPossibleCases(arr) {
        if (arr.length == 1) {
            return arr[0];
        } else {
            var result = [];
            var allCasesOfRest = allPossibleCases(arr.slice(1));  // recur with the rest of array
            for (var i = 0; i < allCasesOfRest.length; i++) {
                for (var j = 0; j < arr[0].length; j++) {
                    result.push(arr[0][j] + " " + allCasesOfRest[i]);
                }
            }
            return result;
        }
    }
</script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.min.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
