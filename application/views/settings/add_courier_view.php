<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    .upload-holder {
        width: 100%;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Add Courier</h3><a href="<?=base_url(). 'ez/settings/courier/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row">
                    <div class="centered push_one eight columns">
                        <center><h2>Add Courier Option</h2></center>
                    </div><Br/>
                </div>
                <div class="row field">
                        <form action="<?=base_url()?>ez/settings/courier/add/" method="post">
                        <div class="push_four fourteen columns">
                            <div class="row">
                                <div class="ten columns">
                                    <span class="txt-label">Courier Name</span>
                                    <input type="text" class="input" name="courier_name" placeholder="Courier Name" />
                                </div>
                            </div>
                            <Br/>
                        </div>

                        <div class="row"><br/><Br/>
                            <div class="centered four columns">
                                <center><input class="medium primary btn" type="submit" name="btn_prod_add" value="Add Courier" style="color: #fff; font: 700 16px 'Open Sans', sans-serif;"></center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br/><br/>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>

    <div class="inventory-alert" style="position: fixed; display: none; bottom: 0; width: 100%; padding: 20px; color: #fff; font: 700 18px 'Open Sans', sans-serif; background: rgba(0, 0, 0, 0.5);">
        <center>Inventory has been successfully updated</center>
    </div>
</div>
<script type="text/javascript">
    $(function() {

        $('input[name="shipping_min_size"]').on('change', function() {
            $('input[name="shipping_max_size"]').attr('min', $(this).val());
        });

        $("input[name='discount_start_date']").on('change', function() {
            $("input[name='discount_end_date']").attr('min', $(this).val());
        });

        $("div[data-tag='discount_limit']").click(function() {
            var action =  $(this).attr('data-action');

            if(action == '2') {
                $('input[name="discount_limit_value"]').hide("fast");
            } else {
                $('input[name="discount_limit_value"]').show("fast");
            }

            $("input[name='discount_limit']").val(action);

            $("div[data-tag='discount_limit']:eq("+(action-1)+")").removeClass('default').addClass('primary');
            $("div[data-tag='discount_limit']:not(':eq("+(action-1)+")')").removeClass('primary').addClass('default');

        });

        $("div[data-tag='discount_type']").click(function() {
            var action =  $(this).attr('data-action');

            if(action == '2') {
                $('input[name="discount_limit"]').hide("fast");
            } else {
                $('input[name="discount_limit"]').show("fast");
            }

            $("input[name='shipping_type']").val(action);

            $("div[data-tag='discount_type']:eq("+(action-1)+")").removeClass('default').addClass('primary');
            $("div[data-tag='discount_type']:not(':eq("+(action-1)+")')").removeClass('primary').addClass('default');

        });

    });
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
