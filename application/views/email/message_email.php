<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <section style="background: #fff; width: 90%; height: auto; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 5px; position: relative; margin: 0 auto;">
        <div class="header" style="background: #1d1d1f; border-top-left-radius: 5px; border-top-right-radius: 5px; width: 100%; position: absolute; top: 0; left: 0; height: auto;">
            <center>
                <img src="http://ez.hairgeek.ph/img/options/main-logo.jpg" alt="" style="margin-top: 20px;">
                <p style="color: #fff; margin-top: -10px; font: 700 16px 'Open Sans', sans-serif;">Message</p>
            </center>
        </div>

        <div class="content" style="margin-top: 20px; padding: 20px;">
            <div class="greetings" style="float: none;">
                <p style=" float: right; font: 700 14px 'Open Sans', sans-serif;">
                    <?=$date?>
                </p>
            </div>
            <br/><br/><Br/>
            <div class="body" style="float: none;  font: 700 14px 'Open Sans', sans-serif;">
                <?=$Message?>
            </div>
        </div>
    </section>
</body>
</html>
