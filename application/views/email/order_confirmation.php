<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <section style="background: #fff; width: 90%; height: auto; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 5px; position: relative; margin: 0 auto;">
        <div class="header" style="background: #1d1d1f; border-top-left-radius: 5px; border-top-right-radius: 5px; width: 100%; position: absolute; top: 0; left: 0; height: auto;">
            <center>
                <img src="http://ez.hairgeek.ph/img/options/main-logo.jpg" alt="" style="margin-top: 20px;">
                <p style="color: #fff; margin-top: -10px; font: 700 16px 'Open Sans', sans-serif;">Order Confimation</p>
            </center>
        </div>

        <div class="content" style="margin-top: 80px; padding: 20px;">
            <div class="greetings" style="float: none;">
                <p style=" float: left; font: 700 14px 'Open Sans', sans-serif;">
                    Dear <?=$this->Customer_Model->get_cutomer_detail_by_id($order[0]['customer_id'])->customer_fname . ' ' . $this->Customer_Model->get_cutomer_detail_by_id($order[0]['customer_id'])->customer_lname?>,
                </p>

                <p style=" float: right; font: 700 14px 'Open Sans', sans-serif;">
                    <?=$order[0]['date_created']?>
                </p>
            </div>
            <br/><br/><Br/>
            <div class="body" style="float: none;  font: 700 14px 'Open Sans', sans-serif;">
                <p>
                    Thank you for placing your order with HairGeek <br/>
                    This e-mail is to confirm your recent order.<Br/><br/>

                    To fulfill you Payment Please proceed to this link.<br/>
                    <a href="<?=base_url()?>cart/fulfill/<?=$order[0]['order_link']?>">Click Here</a>
<br/><br/>
                    <?=$this->Customer_Model->get_cutomer_detail_by_id($order[0]['customer_id'])->customer_fname . ' ' . $this->Customer_Model->get_cutomer_detail_by_id($order[0]['customer_id'])->customer_lname?>
                    <br/><?=$order[0]['order_company']?>
                    <br/><?=$order[0]['order_address']?>
                    <br/><?=$order[0]['order_city']?>, <?=$order[0]['order_zipcode']?>
                    <br/><?=$order[0]['order_phone']?>
                </p>

                <?php foreach ($variant as $key => $row) {
                    $oPrice=$nPrice=$this->Product_Model->get_variant_by_id($row['product_variant_id'])->product_variant_price;
                    $reason=$dType="";
                ?>
                    <?php foreach ($discount as $key => $disc) {
                        if($disc['product_variant_id'] == $row['product_variant_id']) {
                            $dType = ($disc['order_discount_type'] == 1)? 'P ' . $disc['order_discount_value'] : $disc['order_discount_value'] . '%';
                            if($disc['order_discount_type'] == 1) {
                                $nPrice = intval($nPrice)-intval($disc['order_discount_value']);
                            } else {
                                $nPrice = intval($nPrice) - (intval($nPrice)*(intval($disc['order_discount_value'])/100));
                            }

                            $reason = $disc['order_discount_reason'];
                        }
                    } ?>
                        <div><img src="http://ez.hairgeek.ph/img/products/<?=$this->Product_Model->get_variant_by_id($row['product_variant_id'])->product_variant_img?>" width="60" alt="" />

                        </div><div>
                            <span style="font: 700 16px 'Open Sans', sans-serif;"><?=$this->Product_Model->get_product_by_id($this->Product_Model->get_variant_by_id($row['product_variant_id'])->product_id)->product_title?></span><br/>
                            <span><?=$this->Product_Model->get_variant_by_id($row['product_variant_id'])->product_variant_name?></span>

                        </div><div>
                        P <?=($oPrice==$nPrice)? $oPrice : '<small>'. number_format($oPrice, 2) .' / </small> '. number_format($nPrice, 2) . '<br/>Discount '. $dType .'<br/>'.$reason?> x <?=$row['order_content_qty']?>

                    </div><div>
                            P <?=($oPrice==$nPrice)?  number_format($oPrice*intval($row['order_content_qty']),2) : number_format($nPrice*intval($row['order_content_qty']), 2)?>
                        </div>
                        <br/><BR/>
                <?php } ?>
                <?php if (count($order_discount) >= 1): ?>
                    <h3 class="content-title" style="color:#000; float: none;">Discount : <?=($order_discount[0]['order_discount_type'] == 1)? 'P' : ''?> <?=$order_discount[0]['order_discount_value']?><?=($order_discount[0]['order_discount_type'] != 1)? '%' : ''?> (<?=$order_discount[0]['order_discount_reason']?>)</h3>
                <?php endif; ?>
                <h3 class="content-title" style="color:#000; float: none;">Subtotal : P <?=$computation['subtotal']?></h3>
                <h3 class="content-title" style="color:#000; float: none;">Shipping : P <?=$computation['shipping']?></h3>
                <h3 class="content-title" style="color:#000; float: none;">Payment Fee : P <?=$computation['additional']?></h3>
                <h3 class="content-title" style="color:#000; float: none;">Total : P <?=$computation['total']?></h3>

                <p style="font: 400 12px 'Open Sans', sans-serif;">
                    This is a auto generated email. Please Contact us for more info.
                </p>
            </div>
        </div>
    </section>
</body>
</html>
