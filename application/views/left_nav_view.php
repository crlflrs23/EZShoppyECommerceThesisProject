<?php
    $id = $this->session->userdata('admin');
    $account = $this->Account_Model->get_account_by_id($id);
 ?>
<div class="menu-holder" style="position: fixed !important; z-index: 99999999999;">
    <div id="site-menu" style="position: fixed !important; z-index: 99999999999999999;">
        <center>
            <div class="logo-holder"><img src="<?=base_url()?>img/ez-logo.png" alt="" /></div>
            <h3 class="banner-title"><?=$account->account_fname . ' ' . $account->account_lname?></h3>
            <div class="txt-label" style="color: #bfbfbf; font-size: 12px; margin-top: -5px; color: #bfbfbf !important;"><?=$this->Option_Model->get_option_by_value('store_name')->option_value?></div><BR/>
            <a href="<?=base_url()?>ez/login/logout/"><div class="txt-label" style="color: #bfbfbf; font-size: 12px; margin-top: -5px; color: #bfbfbf !important;">Logout</div></a>
            <nav style="margin-top: -10px;">
                <ul class="main-navigation">
                    <li><a href="<?=base_url()?>ez/"><i class="icon-home"> </i>Dashboard</a></li>
                    <li>
                        <a href="#"><i class="icon-tag"> </i>Products</a>
                        <span style="float: right;">+</span>
                        <ul>
                            <li><a href="<?=base_url()?>ez/product/">Products</a></li>
                            <li><a href="<?=base_url()?>ez/product/sale/">Items On Sale</a></li>
                            <li><a href="<?=base_url()?>ez/product/purchase/">Purchase Order</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-download"> </i>Orders</a>
                        <span style="float: right;">+</span>
                        <ul>
                            <li><a href="<?=base_url()?>ez/order/">Orders</a></li>
                            <li><a href="<?=base_url()?>ez/order/cancelled/">Cancelled</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?=base_url()?>ez/customer/"><i class="icon-users"> </i>Customers</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>ez/discount/"><i class="icon-ticket"> </i>Discounts</a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-tag"> </i>Reports</a>
                        <span style="float: right;">+</span>
                        <ul>
                            <li><a href="<?=base_url()?>ez/reports/visitors/">Visitor</a></li>
                            <li><a href="<?=base_url()?>ez/reports/sales/">Sales</a></li>
                            <li><a href="<?=base_url()?>ez/reports/stockin/">Stock In Log</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-cog"> </i>Settings</a>
                        <span style="float: right;">+</span>
                        <ul>
                            <li>General</li>
                            <li><a href="<?=base_url()?>ez/settings/shipping/">Shipping</a></li>
                            <li><a href="<?=base_url()?>ez/settings/courier/">Courier</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </center>
    </div>
    <BR/>
    <button id="menu-expander" class="toggle-nav c-hamburger c-hamburger--htx"><span>toggle menu</span></button>
</div>
