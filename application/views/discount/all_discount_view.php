<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    input[type='checkbox'] {
        width: 16px;
        height: 16px;
        display: block !important;
        -webkit-appearance: checkbox;
    }

    a {
        color: #7997c1;
    }

    a:hover {
        color: #7999e2;
    }
</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Discounts</h3><a href="<?=base_url(). 'ez/discount/create/'?>"><span class="btn-more" style="color:#fff;">Create Discount</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid field">
                <div class="row">
                    <div class="centered push_one eight columns">
                        <center><h2>Manage Discounts</h2>
                        <p>The list of orders has been made.</p></center>
                        <Br/>
                        <form method="post" class="append">
                            <span class="txt-label">Discount Code</span><br/>
                            <input type="text" class="input" name="search_temp" placehoder="Search Product" list="search-datalist">
                            <div class="medium primary btn"><i class="icon-search" style="color: #fff;"></i></div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="centered push_one twelve columns">
                        <table class="paginate" max="50">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th><center><span class="product-title">Discount Code</span></center></th>
                                    <th><center><span class="product-title">Name</span></center></th>
                                    <th><center><span class="product-title">Limit</span></center></th>
                                    <th><center><span class="product-title">Value</span></center></th>
                                    <th><center><span class="product-title">Date Created</span></center></th>
                                    <th><center><span class="product-title">Date Expires</span></center></th>
                                    <th><center><span class="product-title">Action</span></center></th>
                                </tr>
                            </thead>
                            <?php foreach ($discount as $key => $row): ?>
                                <?php
                                    $style="";
                                    if($row->discount_status == '0' || count($this->Discount_Model->get_discount_by_code($row->discount_code)) <= 0) {
                                        $style = "style='color: red;'";
                                    }
                                 ?>
                                <tr class="stocks-row">
                                    <td>
                                        <span class="product-title"  <?=$style?>><?=$row->discount_code?></span>
                                        <span class="text-label"  <?=$style?>></span>
                                    </td>
                                    <td><?=$row->discount_name?></td>
                                    <td><?=$row->discount_limit?></td>
                                    <td>
                                        <span class="product-title"><?=($row->discount_type == '1'? 'P' . $row->discount_value : substr($row->discount_value, 0, -3) . '%') ?></span>
                                        <span class="text-label"></span>
                                    </td>
                                    <td><?=$row->date_created?></td>
                                    <td><?=$row->date_expires?></td>
                                    <td>
                                        <?php if ($row->discount_status == 0 || count($this->Discount_Model->get_discount_by_code($row->discount_code)) <= 0): ?>

                                            <?php else: ?>
                                            <a href="<?=base_url(). 'ez/discount/end/' . $row->discount_code?>/"><span class="btn-more" style="margin-right: 5px !important; float: none;">End Promo</span></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script>

    $(function() {

    });
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
