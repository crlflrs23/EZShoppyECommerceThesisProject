<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');
?>
	<div class="sixteen colgrid main-content">
		<div class="row">
            <center>
                <h1>Search Result for <i><?=$keyword?></i></h1>
            </center>

            <div class="row">
                <ul class="product-holder">
                    <?php foreach ($products as $key => $value): ?>
                        <?php
                            $product_data = $this->Product_Model->get_product_by_id($value->product_id);

                            $meta = $this->Product_Model->get_meta_by_id($value->product_id);
                            $pvname = (strpos($value->product_variant_name, ' '))? explode(' ', $value->product_variant_name) : $value->product_variant_name;
                            $desc = "";

                            foreach ($meta as $metaKey => $metaRow) {
                                if(is_array($pvname)) {
                                    $desc .= $metaRow->product_meta_keyword . ': ' . $pvname[$metaKey] . ' ';
                                } else {
                                    $desc =  $metaRow->product_meta_keyword . ': ' . $pvname . ' ';
                                }
                            }

                            $img = $value->product_variant_img;
                            if(strpos($img, ',') >= 1) {
                                $img_ex = explode(',', $img);
                                $img = $img_ex[0];
                            }
                         ?>
                        <li class="product">
        					<div class="content">
        						  <center>
        							  <div class="product-img">
        							  	<img src="<?=base_url()?>img/products/<?=$img?>" alt="">
        							  </div>
        							  <br/>
                                      <a href="<?=base_url() . 'product/desc/' . $value->product_variant_id . '/' . strtolower(str_replace(' ', '-', $product_data->product_title . ' ' . $value->product_variant_name)) . '/'?>">
        							  <div class="product-details">
        								<span class="product-title"><?=$product_data->product_title?><br/>(<?=$desc?>)</span>
        								<div class="rotate-content">
        									<ul class="content-rotate">
        										<li>
        											<?php if ($value->product_variant_compare_price == "0.00"): ?>
        											    <span class="product-price">P <?=number_format($value->product_variant_price, 2)?></span>
        											<?php else: ?>
                                                        <span class="product-price sale">P <?=number_format($value->product_variant_price, 2)?></span>
                                                        <span class="product-price old">P <?=number_format($value->product_variant_compare_price, 2)?></span>
        											<?php endif; ?>
        										</li>
        										<li>
            										<a href="<?=base_url()?>cart/add/<?=$value->product_variant_id?>/?redirect_to=<?="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"><button><i class="fa fa-plus"> </i> Add to Cart</button></a>
        										</li>
        									</ul>
        								</div>
        							  </div>
                                      </a>
        						  </center>
        					</div>
        				</li>
                    <?php endforeach; ?>
                <ul>
            </div>
        </div>
	</div>

<?php
    $this->load->view('main/footer_view');
?>
