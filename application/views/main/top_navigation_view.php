<?php

    $ip = $this->ezclient->get_ip();
    if(count($this->Cart_Model->get_cart_by_ip($ip)) >= 1) {
        $this->session->set_userdata('group', $this->Cart_Model->get_cart_by_ip($ip)->cart_group);
    }

    if($this->session->userdata('group') !== null) {
        $group = $this->session->userdata('group');
        $notify_count = 0;
        foreach ($this->Cart_Model->get_cart_content($this->ezclient->get_ip(), $group) as $key => $value) {
            $notify_count += $value->cart_quantity;
        }
    }

    if($this->session->userdata('visit') === null) {
        $ip = "182.18.238.219";
        
        $ip_details = file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip);

		if($ip_details != "") {
		    $ipd = json_decode($ip_details);

			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$visitor_data = array(
				"visitor_ip" => $ip,
				"visitor_city" => isset($ipd->geoplugin_city)? $ipd->geoplugin_city : "-",
				"visitor_region" => $ipd->geoplugin_regionName != "" ? $ipd->geoplugin_regionName : "-",
				"visitor_country_code" => $ipd->geoplugin_countryCode,
				"visitor_referer" => (isset($_SERVER["HTTP_REFERER"]))? $_SERVER["HTTP_REFERER"] : '',
				"visitor_agent" => $_SERVER["HTTP_USER_AGENT"],
				"visitor_device" => $this->ezclient->is_mobile($_SERVER["HTTP_USER_AGENT"]),
				"date_created" => $time
			);

			$this->Customer_Model->insert_visitor($visitor_data);
			$this->session->set_userdata('visit', "1");
		}
    }

    $this->Reports_Model->reset_all();

 ?>

<nav>
    <div class="sixteen colgrid">

        <div class="mobile-bar">
            <div class="">
                <ul class="mobile-nav">
                    <li class="tiles"><img src="<?=base_url()?>assets/img/main-logo.png" alt=""></li>
                    <li class="tiles">
                        <span><a href="<?=base_url()?>cart/"><i class="error-icon fa fa-shopping-bag"> </i> <?=isset($notify_count)?  (($notify_count > 9)? '9+' : $notify_count) : ""?></a></span>
                        <span><i id="mm-trig" class="fa fa-bars"> </i></span>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="menu-toggle" style="">
                <ul class="menu-toggle-content">
                    <li><a href="<?=base_url()?>">Shop</a></li>
                    <li><a href="<?=base_url() . 'feedback/'?>">Feedback</a></li>
                    <li><a href="<?=base_url() . 'shop/contact/'?>">Inquire</a></li>
                    <li class="switch" gumby-trigger="#login_modal"><a href="#"><i class="error-icon fa fa-user"> </i> Account</a></li>
                    <li class="switch" gumby-trigger="#search_modal"><i class="error-icon fa fa-search"> </i> Search</li>
                </ul>
            </div>
        </div>

        <div class="row main-bar">

            <div class="two columns main-logo">
                <img src="<?=base_url()?>assets/img/main-logo.png" width="110"  alt="">
            </div>

            <div class="eight columns">
                <ul class="main-menu">
                    <li><a href="<?=base_url()?>">Shop</a></li>
                    <li><a href="<?=base_url()?>feedback/">Feedback</a></li>
                    <li><a href="<?=base_url() . 'shop/contact/'?>">Inquire</a></li>
                </ul>
            </div>

            <div class="six columns">
                <ul class="side-menu main-menu">
                    <li class="switch" gumby-trigger="#search_modal"><a href=""><i class="fa fa-search"></i></a></li>
                    <?php if ($this->session->userdata('customer') === null): ?>
                        <li><a href="" class="switch" gumby-trigger="#login_modal"><i class="fa fa-user" >  </i> Log In / Sign Up</a></li>
                    <?php else: ?>
                        <?php $customer = $this->session->userdata('customer'); ?>
                        <li><a href="<?=base_url()?>customer/"><i class="fa fa-user"> </i> <span class="user-name">Hi, <?=$customer['fullname']?>!</span></a></li>
                    <?php endif; ?>
                    <li><a href="<?=base_url()?>cart/"><i class="fa fa-shopping-bag"></a></i>
                    <?php if ($this->session->userdata('group') !== null): ?><div class="bag-notify"><?=($notify_count > 9)? '9+' : $notify_count;?></div></li><?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</nav>
