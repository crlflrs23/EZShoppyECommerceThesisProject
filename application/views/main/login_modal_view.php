<?php if ($this->session->userdata('customer') === null): ?>
    <div class="modal" id="login_modal">
        <div class="content">
            <a class="close switch" gumby-trigger="|#login_modal"><i class="icon-cancel" /></i></a>
            <div class="row">
                <div class="five columns text-center field">
                    <h3><i class="fa fa-user"> </i> Login</h3>
                    <form action="<?=base_url()?>login/" method="post">
                        <span class="text-label input-label">Email</span>
                        <input type="text" class="input" name="txt_email" placeholder="E-mail"><br/>
                        <span class="text-label input-label">Password</span>
                        <input type="password" class="input" name="txt_password" placeholder="Password"><br/><br/>
                        <input type="hidden" name="txt_url" value="<?="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" />
                        <input type="submit"  class="medium primary btn" name="login" value="Login" />
                    </form>
                </div>

                <div class="two columns login-divide">
                    <center>
                        <div style="border-left: 1px solid #e1e1e1; height: 200px; width: 1px; margin: 20px auto;"></div>
                    </center>
                </div>

                <div class="five columns text-center field">
                    <h3><i class="fa fa-user-plus"> </i> Sign Up</h3>
                    <form action="<?=base_url()?>customer/signup/" method="post">
                        <div class="row">
                            <div class="six columns">
                                <span class="text-label input-label">First Name <span class="text-indicator">*</span></span>
                                <input type="text" class="input" name="txt_fname" placeholder="First Name"><br/>
                            </div>
                            <div class="six columns">
                                <span class="text-label input-label">Last Name <span class="text-indicator">*</span></span>
                                <input type="text" class="input" name="txt_lname" placeholder="Last Name"><br/>
                            </div>
                        </div>
                        <span class="text-label input-label">Email <span class="text-indicator">*</span></span>
                        <input type="text" class="input" name="txt_email" placeholder="E-mail"><br/>
                        <span class="text-label input-label">Password <span class="text-indicator">*</span></span>
                        <input type="password" class="input" name="txt_password" placeholder="Password"><br/>
                        <span class="text-label input-label">Confirm Password <span class="text-indicator">*</span></span>
                        <input type="password" class="input" name="txt_re_password" placeholder="Confirm Password"><br/><br/>
                        <input type="hidden" name="txt_url" value="<?="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" />
                        <input type="submit"  class="medium primary btn" name="login" value="Sign Up" />
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
