<footer>
    <div class="sixteen colgrid">
        <div class="row">
            <ul class="footer-content">
                <li class="tiles">
                    <h6>Important Links</h6>
                    <div class="sub-content">
                        <ul>
                            <li>Order Status</li>
                            <li>Payment</li>
                            <li>Returns</li>
                            <li>Contact</li>
                        </ul>
                    </div>
                </li>
                <li class="tiles field">
                    <h6>Subscribe</h6>
                    <form action="" method="post">
                        <input type="text" name="sub_email" class="input xxwide" placeholder="Email" style="margin-bottom: 4px;" /><br>
                        <input type="text" name="sub_fullname" class="input wide" placeholder="Full Name" />
                        <input type="submit" name="sub_submit" class="narrow medium info btn" value="Submit" style="margin-left: 4px;" />
                    </form>

                    <div class="sub-content">
                        You can get Hairgeek newsletter, promotions, and latest offers.<br/>
                        You may <a href="#">unsubscribe</a> anytime.
                    </div>
                </li>
                <li class="tiles">
                    <h6>Follow Us On</h6>
                    <div class="sub-content">
                        <a target="_blank" href="<?=$this->Option_Model->get_option_by_value('facebook')->option_value?>"><i class="social fa fa-facebook"> </i></a>
                        <a target="_blank" href="<?=$this->Option_Model->get_option_by_value('instagram')->option_value?>"><i class="social fa fa-instagram"> </i></a>
                    </div>

                    <Br/>
                    <h6>Ez Solution</h6>
                </li>
                <li class="tiles">
                    <h6>Quick Information</h6>
                    <div class="sub-content">
                        <table>
                            <tr>
                                <td><i class="fa fa-map-marker"> </i> </td>
                                <td><?=$this->Option_Model->get_option_by_value('footer_address')->option_value?></td>
                            </tr>
                            <tr>
                                <td><i class="fa fa-mobile"> </i></td>
                                <td><?=$this->Option_Model->get_option_by_value('footer_cellphone')->option_value?></td>
                            </tr>
                            <tr>
                                <td><i class="fa fa-phone"> </i></td>
                                <td><?=$this->Option_Model->get_option_by_value('footer_telephone')->option_value?></td>
                            </tr>
                            <tr>
                                <td><i class="fa fa-heart"> </i></td>
                                <td><?=$this->Option_Model->get_option_by_value('footer_email')->option_value?></td>
                            </tr>
                        </table>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</footer>

<div class="modal" id="newsletter_modal">
    <div class="content">
        <a class="close switch" gumby-trigger="|#newsletter_modal"><i class="icon-cancel" /></i></a>

        <div class="row">
            <center><h3>Thank you for Subscribing to HairGeek, you'll be receiving an email in our promotion and discounts.</h3></center>
        </div>
    </div>
</div>

<div class="switch" id="newletter_activate" gumby-trigger="#newsletter_modal" style="display: none;"></div>

</body>

<script>
    $(function() {
        $('button[name="btn_search"]').click(function() {
    		window.location = "<?=base_url()?>shop/search/" + $("input[name='txt_search']").val() + "/";
    	});

        $('input[name="sub_submit"]').click(function() {
            var email = $('input[name="sub_email"]').val(),
                fullname = $('input[name="sub_fullname"]').val();

            $.post('<?=base_url()?>shop/subscribe/', {email:email, fullname:fullname}, function(response) {
                if(parseInt(response) >= 1) {
                    $('#newletter_activate').trigger('click');
                }
            });

            return false;
        });

    });
</script>
<script src="<?=base_url()?>assets/js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>assets/js/libs/gumby.js"></script>
<script src="<?=base_url()?>assets/js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>assets/js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>assets/js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>assets/js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>assets/js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>assets/js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>assets/js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>assets/js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>assets/js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>assets/js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>assets/js/plugins.js"></script>
<script src="<?=base_url()?>assets/js/main.js"></script>

</html>
