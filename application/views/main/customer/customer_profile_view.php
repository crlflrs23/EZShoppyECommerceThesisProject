<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');

    $customer = $this->session->userdata('customer');
?>
	<div class="sixteen colgrid main-content">
		<div class="row">
            <center>
                <h1>Customer Account</h1>
                <p>
                    Hello <?=$customer['fullname']?> (<a href="<?=base_url()?>login/logout/">Sign out</a>). You can view your recent orders,<br/>edit shipping address and edit your personal information.
                </p>
            </center>

            <div class="five columns address-holder">
                <h3>My Address</h3>
                <p>
                    The following address will be used on the checkout page by default.
                </p>

                <a href="<?=base_url()?>customer/edit/"><button><i class="fa fa-pencil"> </i> Edit</button></a>
            </div>
        </div>
        <br/>
        <?php if (isset($_REQUEST['action'])): ?>
            <?php if ($_REQUEST['action'] == md5('cancel')): ?>
                <div class="row">
                    <div class="success alert" style="padding: 10px;"><center>Order # <?=$_REQUEST['id']?> has been successfuly cancelled.</center></div>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <Br/>
        <div class="row">
            <center><h3>My Recent Orders</h3></center>

            <div class="centered twelve columns cart-holder">
                <table>
                    <tr>
                        <td ><span class="text-label">Order #</span></td>
                        <td ><span class="text-label">Total Amount</span></td>
                        <td ><span class="text-label">Status</span></td>
                        <td ><span class="text-label">Action</span></td>
                    </tr>
                    <?php foreach ($orders as $key => $value): ?>
                        <tr>
                            <td>#<?=$value->order_id?></td>
                            <td>P <?=number_format($value->order_total, 2)?></td>
                            <td>
                                <?php if ($value->order_payment_status != 2): ?>
                                    <?php if ($value->order_payment_status == 0): ?>
                                        <span style="color: red;">Pending</span>
                                    <?php else: ?>
                                        <span style="color: #FFCC00;">On Process</span>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <span style="color: green;">Paid</span>
                                <?php endif; ?>
                            </td>
                            <td style="text-align: center;">
                                <?php if ($value->order_payment_status == 0): ?>
                                    <a href="<?=base_url()?>cart/cancel/<?=$value->order_id?>/<?=$value->order_link?>/"><button name="button" style="margin-bottom: 5px;">Cancel Order</button></a>
                                <?php endif; ?>

                                <?php if ($value->order_payment_status <= 0): ?>
                                    <a href="<?=base_url()?>cart/fulfill/<?=$value->order_link?>/"><button name="button">Confirm Payment</button></a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
	</div>

<?php
    $this->load->view('main/footer_view');
?>
