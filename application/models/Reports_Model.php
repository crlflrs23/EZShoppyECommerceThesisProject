<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_Model extends CI_Model {

	function __construct(){
	parent::__construct();
	date_default_timezone_set('Asia/Manila');
	}

  // Sales

	  function sales_per_day() {
		$this->db->select('SUM(order_total) as sales_per_week, date_modified, COUNT(date_modified) as sales_count', false);
		$this->db->where('order_payment_status', '2');
		$this->db->group_by('DAY(date_modified)', false);
		return $this->db->get('cs_orders')->result();
	  }

	  function sales_per_day_pending() {
		$this->db->select('SUM(order_total) as sales_per_week, date_modified, COUNT(date_modified) as sales_count', false);
		$this->db->where('order_payment_status !=', '2');
		$this->db->group_by('DAY(date_modified)', false);
		return $this->db->get('cs_orders')->result();
	  }

	  function sales_per_year_rep() {
		$this->db->select('SUM(order_total) as sales_per_week, date_modified, COUNT(date_modified) as sales_count', false);
		$this->db->where('order_payment_status', '2');
		$this->db->group_by('YEAR(date_modified)', false);
		return $this->db->get('cs_orders')->result();
	  }

	  function sales_per_week_rep($year) {
		$this->db->select('SUM(order_total) as sales_per_week, date_modified, COUNT(date_modified) as sales_count', false);
		$this->db->where('order_payment_status', '2');
		$this->db->where('YEAR(date_modified)', $year);
		$this->db->group_by('WEEK(date_modified)', false);
		return $this->db->get('cs_orders')->result();
	  }

	  function sales_per_week_rep_all() {
		$this->db->select('SUM(order_total) as sales_per_week, date_modified, COUNT(date_modified) as sales_count', false);
		$this->db->where('order_payment_status', '2');
		$this->db->group_by('WEEK(date_modified)', false);
		return $this->db->get('cs_orders')->result();
	  }

	  function sales_per_month_rep($year) {
		$this->db->select('SUM(order_total) as sales_per_week, date_modified, COUNT(date_modified) as sales_count', false);
		$this->db->where('order_payment_status', '2');
		$this->db->where('YEAR(date_modified)', $year);
		$this->db->group_by('MONTH(date_modified)', false);
		return $this->db->get('cs_orders')->result();
	  }

	  function sales_per_month_rep_all() {
		$this->db->select('SUM(order_total) as sales_per_week, date_modified, COUNT(date_modified) as sales_count', false);
		$this->db->where('order_payment_status', '2');
		$this->db->group_by('MONTH(date_modified)', false);
		return $this->db->get('cs_orders')->result();
	  }

	  function sales_per_week($week) {
	    $this->db->select("(SELECT SUM(cs_orders.order_total) FROM cs_orders WHERE WEEK(cs_orders.date_modified) = '$week' AND cs_orders.order_payment_status = '2' GROUP BY WEEK(cs_orders.date_modified)) as sales_per_week", false);
	    return $this->db->get('cs_orders')->row();
	  }

	  function sales_per_month($month) {
	    $this->db->select("(SELECT SUM(cs_orders.order_total) FROM cs_orders WHERE MONTH(cs_orders.date_modified) = '$month' AND cs_orders.order_payment_status = '2' GROUP BY MONTH(cs_orders.date_modified)) as sales_per_week", false);
	    return $this->db->get('cs_orders')->row();
	  }

	  function sales_per_year($year) {
	    $this->db->select("(SELECT SUM(cs_orders.order_total) FROM cs_orders WHERE YEAR(cs_orders.date_modified) = '$year' AND cs_orders.order_payment_status = '2' GROUP BY YEAR(cs_orders.date_modified)) as sales_per_week", false);
	    return $this->db->get('cs_orders')->row();
	  }

	// Orders
		function order_pending() {
			$this->db->order_by('date_modified', 'desc');
			$this->db->limit(5);
			return $this->db->get('cs_orders')->result();
		}

	// Product
		function get_all_product_on_sale() {
			$this->db->select('*');
			$this->db->from('cs_product_sale as a');
			$this->db->join('cs_product_variants as b', 'a.product_variant_id = b.product_variant_id');
			$this->db->where('a.product_sale_status', '1');
			$this->db->limit('5');
			return $this->db->get()->result();
		}

		function check_product_on_sale($product_variant_id) {
			$this->db->select('*');
			$this->db->from('cs_product_sale as a');
			$this->db->join('cs_product_variants as b', 'a.product_variant_id = b.product_variant_id');
			$this->db->where('a.product_sale_status', '1');
			$this->db->where('a.product_variant_id', $product_variant_id);
			$this->db->limit('5');
			return $this->db->get()->result();
		}

        function get_sold_count($sale_id) {
            $query = $this->db->query("
                SELECT SUM(cs_order_content.order_content_qty) as sold_count
                FROM cs_product_sale
                JOIN cs_product_variants ON cs_product_sale.product_variant_id = cs_product_variants.product_variant_id
                JOIN cs_order_content ON cs_product_variants.product_variant_id = cs_order_content.product_variant_id
                JOIN cs_orders ON cs_orders.order_id = cs_order_content.order_content_id
                WHERE cs_orders.date_created > cs_product_sale.product_sale_start
                AND cs_orders.date_created < cs_product_sale.product_sale_expires
                AND cs_product_sale.product_sale_id = '$sale_id'
            ");

            return $query->row();
        }

		function get_product_w_inventory($limit) {
			$this->db->select('*');
			$this->db->from('cs_product_variants as a');
			$this->db->join('cs_inventory as b', 'a.product_variant_sku = b.inventory_sku');
			$this->db->order_by('b.inventory_stocks', 'asc');

			if ($limit != 0) {
				$this->db->limit($limit);
			}

			return $this->db->get()->result();
		}

		function get_sold_count_product($product_id) {
            $query = $this->db->query("
                SELECT SUM(cs_order_content.order_content_qty) as sold_count
                FROM cs_product_variants
                JOIN cs_order_content ON cs_product_variants.product_variant_id = cs_order_content.product_variant_id
                JOIN cs_orders ON cs_orders.order_id = cs_order_content.order_content_id
				WHERE cs_product_variants = '$product_id'
            ");

            return $query->row();
        }

    // Searches

        function get_search_stat() {
            $this->db->order_by('search_statistic_count', 'desc');
            $this->db->limit('5');
            return $this->db->get('cs_search_statistics')->result();
        }

	// Visitor

		function get_all_search() {
			$this->db->select("*, count(*) as count");
			$this->db->from("cs_visitors");
			$this->db->group_by('DATE(date_created)');
			return $this->db->get()->result();
		}

		function get_all_search_group($group) {
			$this->db->select("*, count(*) as count");
			$this->db->from("cs_visitors");
			$this->db->order_by('count', 'desc');
			$this->db->group_by($group);
			return $this->db->get()->result();
		}

	// Get Trasfer Log

		function get_trasfer_log() {
			return $this->db->get('cs_inventory_transfer')->result();
		}


	// resetter

		function reset_all() {
			$time = time();
			foreach ($this->get_all_product_on_sale() as $key => $value) {
				if(strtotime($value->product_sale_expires) >= $time) {
					$vi = $this->Product_Model->get_variant_by_id($value->product_variant_id);
					$product_data = array(
						"product_variant_price" => $vi->product_variant_compare_price,
						"product_variant_compare_price" => "0.00"
					);

					$this->Product_Model->update_variant($product_data, $vi->product_variant_id);

					$sale_data = array(
						"product_sale_status" => "0"
					);
					$this->Product_Model->update_sale($sale_data, $value->product_sale_id);
				}
			}

		}

}
