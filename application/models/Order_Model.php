<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_Model extends CI_Model {

    function __construct(){
	parent::__construct();
	}

    function insert_order($data) {
        $this->db->insert('cs_orders', $data);
        return $this->db->insert_id();
    }

    function get_all_orders() {
        $this->db->where('order_status !=', '-1');
        $this->db->order_by('date_created', 'desc');
        return $this->db->get('cs_orders')->result();
    }

    function get_all_cancelled_orders() {
        $this->db->where('order_status =', '-1');
        $this->db->order_by('date_created', 'desc');
        return $this->db->get('cs_orders')->result();
    }

    function get_order_by_id($order_id) {
        $this->db->where('order_id', $order_id);
        return $this->db->get('cs_orders')->row();
    }

    function get_order_by_group($group) {
        $this->db->where('order_link', $group);
        return $this->db->get('cs_orders')->row();
    }

    function update_order($data, $order_id){
        $this->db->where('order_id', $order_id);
        $this->db->update('cs_orders', $data);
        return $this->db->affected_rows();
    }

    // Order Discount

        function insert_order_discount($data) {
            $this->db->insert('cs_order_discount', $data);
            return $this->db->insert_id();
        }

        function get_order_discount($order_id, $vid, $type) {
            $this->db->where("order_id", $order_id);
            $this->db->where("product_variant_id", $vid);
            $this->db->where("order_discount_to", $type);
            return $this->db->get('cs_order_discount')->row();
        }

        function delete_order_discount($order_id, $type) {
            $this->db->where("order_discount_to", $type);
            $this->db->where('order_id', $order_id);
            $this->db->delete('cs_order_discount');
            return $this->db->affected_rows();
        }

    // Fulfillment
        function insert_order_fulfillment($data) {
            $this->db->insert('cs_order_fulfillment', $data);
            return $this->db->insert_id();
        }

    // Order Content

        function insert_order_content($data) {
            $this->db->insert('cs_order_content', $data);
            return $this->db->insert_id();
        }

        function get_order_content_by_id($order_id) {
            $this->db->where('order_id', $order_id);
            return $this->db->get('cs_order_content')->result();
        }

        function delete_order_content($order_id) {
            $this->db->where('order_id', $order_id);
            $this->db->delete('cs_order_content');
            return $this->db->affected_rows();
        }

    // Proof

        function check_if_has_proof($order_id) {
            $this->db->where('order_id', $order_id);
            $this->db->order_by('order_proof_id', 'desc');
            return $this->db->get('cs_order_proof')->row();
        }

        function insert_proof($data) {
            $this->db->insert('cs_order_proof', $data);
            return $this->db->insert_id();
        }

    // History

        function insert_order_history($data) {
            $this->db->insert('cs_order_history', $data);
            return $this->db->insert_id();
        }

        function get_order_history($order_id) {
            $this->db->where('order_id', $order_id);
            return $this->db->get('cs_order_history')->result();

        }

    // meta

        function insert_meta($data) {
            $this->db->insert('cs_order_meta', $data);
            return $this->db->insert_id();
        }

        function get_all_meta($order_id) {
            $this->db->where('order_id', $order_id);
            return $this->db->get('cs_order_meta')->result();
        }

        function get_all_meta_value($order_id, $keyword) {
            $this->db->where('order_id', $order_id);
            $this->db->where('keyword', $keyword);
            return $this->db->get('cs_order_meta')->row();
        }

        function get_payment_instruction($value) {
            $this->db->where('payment_method_name', $value);
            return $this->db->get('cs_payment_method')->row();
        }

    // Purchase Order

        function insert_purchase($data) {
            $this->db->insert('cs_purchase_order', $data);
            return $this->db->insert_id();
        }

        function insert_purchase_order_content($data) {
            $this->db->insert('cs_purchase_order_content', $data);
            return $this->db->insert_id();
        }

        function get_all_purchase() {
            return $this->db->get('cs_purchase_order')->result();
        }

        function get_purchase_details($purchase_id) {
            $this->db->where('purchase_order_id', $purchase_id);
            return $this->db->get('cs_purchase_order')->row();
        }

        function get_purchase_content($purchase_id) {
            $this->db->where('purchase_order_id', $purchase_id);
            return $this->db->get('cs_purchase_order_content')->result();
        }

        function update_purchase_order($id, $data) {
            $this->db->where('purchase_order_id', $id);
            $this->db->update('cs_purchase_order', $data);
            return $this->db->affected_rows();
        }

    // Customers Sake

        function get_all_customer_order($customer_id) {
            $this->db->where('customer_id', $customer_id);
            $this->db->where('order_payment_status', '2');
            $this->db->order_by('date_created', 'desc');
            return $this->db->get('cs_orders')->result();
        }

        function get_all_customer_order_all($customer_id) {
            $this->db->where('customer_id', $customer_id);
            $this->db->where('order_status !=', '-1');
            $this->db->order_by('date_created', 'desc');
            return $this->db->get('cs_orders')->result();
        }

        function get_customer_last_order($customer_id) {
            $this->db->where('customer_id', $customer_id);
            $this->db->where('order_payment_status', '2');
            $this->db->order_by('date_created', 'desc');
            $this->db->limit(1);
            return $this->db->get('cs_orders')->row();
        }

        function get_total_spent($customer_id) {
            $this->db->select('SUM(`order_total`) as total');
            $this->db->from('cs_orders');
            $this->db->where('customer_id', $customer_id);
            $this->db->where('order_payment_status', '2');
            $this->db->group_by('customer_id');
            return $this->db->get()->row();
        }

        function get_avg_spent($customer_id) {
            $this->db->select('AVG(`order_total`) as total');
            $this->db->from('cs_orders');
            $this->db->where('customer_id', $customer_id);
            $this->db->where('order_payment_status', '2');
            $this->db->group_by('customer_id');
            return $this->db->get()->row();
        }

}
